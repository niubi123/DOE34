package cn.doitedu.doe.flume;

import org.apache.commons.lang.math.RandomUtils;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.util.List;
import java.util.Map;

/**
 * @Date: 22.10.23
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 * 下游agent  会根据选择器的规则 将source中的Event 分配到不桶的channel中
 *
 */
public class MySelector implements Interceptor {


    @Override
    public void initialize() {

    }

    @Override
    public Event intercept(Event event) {
        Map<String, String> mp = event.getHeaders();
        mp.put("state" , RandomUtils.nextInt(2)+"");
        return event;
    }

    @Override
    public List<Event> intercept(List<Event> list) {
        for (Event event : list) {
            intercept(event) ;
        }
        return list;
    }

    @Override
    public void close() {

    }
     //cn.doitedu.doe.flume.MySelector$MySelectorBuilder
    public static  class MySelectorBuilder implements  Interceptor.Builder{

        @Override
        public Interceptor build() {
            return new MySelector();
        }

        @Override
        public void configure(Context context) {

        }
    }
}
