package cn.doitedu.doe.flume;

import cn.doitedu.doe.beans.LogBean;
import com.alibaba.fastjson.JSON;
import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @Date: 22.10.23
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 *   抽取日志中的时间戳  转换成日期格式的字符串
 *   将日期格式的字符串  设置在Event的Header中
 */
public class MyInterceptor implements Interceptor {
    String  key ;
    public MyInterceptor(){}
    public  MyInterceptor(String key){
        this.key = key ;
    }
    SimpleDateFormat sdf = null ;
    /**
     * 拦截之前执行一次
     */
    @Override
    public void initialize() {
         sdf = new SimpleDateFormat("yyyy-MM-dd");
    }
    /**
     * 拦截每条日志 处理
     * @param event
     * @return
     */
    @Override
    public Event intercept(Event event) {
        try {
            //抽取日志中的时间戳  转换成日期格式的字符串
            //获取body  body中存储的是日志数据
            String line = new String(event.getBody());
            // 解析日志中的时间戳
            LogBean logBean = JSON.parseObject(line, LogBean.class);
            long ts = logBean.getTimeStamp();
            // 将时间戳转换成日期格式的字符串
            String dateStr = sdf.format(new Date(ts));
            // 获取头对象 将日期格式的字符串设置在头信息中
            event.getHeaders().put("log_date" ,dateStr) ;
            // 将处理后的数据 Event返回
            return event;
        }catch (Exception e){

        }
        return null ;
    }
    /**
     * 拦截多条数据处理
     * @param list
     * @return
     */
    @Override
    public List<Event> intercept(List<Event> list) {
        for (Event event : list) {
            intercept(event) ;
        }
        return list;
    }
    /**
     * 处理数据之后指定一次方法
     */
    @Override
    public void close() {
        sdf = null ;
    }
  // cn.doitedu.doe.flume.MyInterceptor$MyInterceptorBuilder
    public  static  class MyInterceptorBuilder implements  Interceptor.Builder{
        String key = null ;
        @Override
        public Interceptor build() {
            return new MyInterceptor(key);
        }
        /**
         * 和 用户的配置交互的方法
         *  获取用户的配置信息
         * @param context
         */
        @Override
        public void configure(Context context) {
            // 用户想 设置的日期格式对应的key
             key = context.getString("key");
        }
    }
}

