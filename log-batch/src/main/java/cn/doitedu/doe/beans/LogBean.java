package cn.doitedu.doe.beans;

/**
 * @Date: 22.10.23
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
public class LogBean {
    /**
     * 时间戳  和json格式日志中的属性名一致
     */
    private  long  timeStamp ;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "LogBean{" +
                "timeStamp=" + timeStamp +
                '}';
    }
}
