#!/bin/bash
DT=$(date  -d'-1 days'  +%Y-%m-%d)
if [ $1 ]
  then   DT=$1
fi
# 配置系统环境变量
export  JAVA_HOME=/opt/apps/jdk1.8.0_191
export  HADOOP_HOME=/opt/apps/hadoop-3.1.1
export  HIVE_HOME=/opt/apps/hive-3.1.2
export  SPARK_HOME=/opt/apps/spark-3.1.3
#  将spark程序 提交到yarn
${SPARK_HOME}/bin/spark-submit  --master yarn  \
--name  session_spliter \
--class com.doitedu.process.C01_Process_Step01 \
--deploy-mode  cluster \
--executor-memory 2G \
--num-executors  4 \
--executor-cores 2 \
/doe/jars/process-data.jar  \
${DT}

