#!/bin/bash
export HADOOP_HOME=/opt/apps/hadoop-3.1.1
# 清理HDFS上的数据
${HADOOP_HOME}/bin/hdfs dfs  -rm -r  hdfs://doitedu01:8020/doe/*

# $? 返回上个命令的执行结果  0正常  非0 失败
if [ $? -eq 0 ]
  then  echo  "清理HDFS上的数据成功...."
  else  echo "清理HDFS上的数据失败...."
fi
# 清理本地的数据  偏移量  channel的本地数据  checkpoint文件夹
# 清理所有机器上  flume运行是产生的中间结果数据
for  hostname  in doitedu01  doitedu02  doitedu03
do
  echo  "正在清理 ${hostname} 机器上的数据... ..."
  ssh $hostname  "rm -rf /opt/flume_data  ;
  if [ $? -eq 0 ]
    then  echo  "${hostname}  清理本地上的数据成功...."
    else  echo  "${hostname}  清理本地上的数据失败...."
  fi"
done




