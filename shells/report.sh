#!/bin/bash

#################################
# 每天上报自己机器的各种日志类型条数信息
# ###############################
DATE=$(date  -d'-1 days'  +%Y-%m-%d)

CNT1=$(cat /doe/data/logs/applogs/*${DATE}.log | wc -l)
# CNT1=$(cat /doe/data/logs/applogs/${DATE}/*| wc -l)
LOGTYPE1=applogs
curl http://windows:8080/collectLogCount -X POST  -d"{\"hostName\":\"${HOSTNAME}\",\"logType\":\"${LOGTYPE1}\" ,\"date\":\"${DATE}\" ,\"cnt\":\"${CNT1}\"}" -H "Content-Type: application/json"

CNT2=$(cat /doe/data/logs/weblogs/*${DATE}.log | wc -l)
LOGTYPE2=weblogs
curl http://windows:8080/collectLogCount -X POST  -d"{\"hostName\":\"${HOSTNAME}\",\"logType\":\"${LOGTYPE2}\" ,\"date\":\"${DATE}\" ,\"cnt\":\"${CNT2}\"}" -H "Content-Type: application/json"

CNT3=$(cat /doe/data/logs/wxlogs/*${DATE}.log | wc -l)
LOGTYPE3=wxlogs
curl http://windows:8080/collectLogCount -X POST  -d"{\"hostName\":\"${HOSTNAME}\",\"logType\":\"${LOGTYPE3}\" ,\"date\":\"${DATE}\" ,\"cnt\":\"${CNT3}\"}" -H "Content-Type: application/json"