#!/bin/bash
DATE=$(date  -d'-1 days'  +%Y-%m-%d)
if  [  $1 ]
then DATE=$1
fi

LOGTYPE1=applogs
# 是否去重
LOCAL_APPLOG_COUNT=$(curl http://windows:8080/getLocalLogCount -X POST  -d"{\"hostName\":\"\",\"logType\":\"${LOGTYPE1}\" ,\"date\":\"${DATE}\" ,\"cnt\":\"\"}" -H "Content-Type: application/json")
HDFS_APPLOG_COUNT=$(curl http://windows:8080/getHDFSLogCount -X POST  -d"{\"hostName\":\"\",\"logType\":\"${LOGTYPE1}\" ,\"date\":\"${DATE}\" ,\"cnt\":\"\"}" -H "Content-Type: application/json")

if [ ${LOCAL_APPLOG_COUNT} -lt ${HDFS_APPLOG_COUNT} ]
# 如无数据是否去重 , 将原始数据加载到指定的分区下
  hive -e "alter table doe34.doe_app_log_raw add partition(dt='${DATE}') location '/doe/applogs/${DATE}/'"
  then   echo "需要去重...."
  # 去重  使用sql  去重后  将去重后的数据 覆盖到原来的分区中
  hive -e "set hive.exec.compress.output = true;
            set mapred.output.compression.codec = org.apache.hadoop.io.compress.GzipCodec;
            insert overwrite table doe34.doe_app_log_raw partition(dt='${DATE}')
            select
            line
            from
            doe34.doe_app_log_raw
            where  dt = '${DATE}'
            group by line  ;"

     if [  $? -eq 0 ]
         then   echo "成功   发送邮件  注册邮箱 "
         else   echo  "失败    发送邮件"
      fi

  else   echo "不需要去重...."
fi
