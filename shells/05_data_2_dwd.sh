#!/bin/bash
DT=$(date  -d'-1 days'  +%Y-%m-%d)
if [ $1 ]
  then   DT=$1
fi
# 配置系统环境变量
export  JAVA_HOME=/opt/apps/jdk1.8.0_191
export  HADOOP_HOME=/opt/apps/hadoop-3.1.1
export  HIVE_HOME=/opt/apps/hive-3.1.2

# 1)  用户注册信息表 每天使用sqoop增量导入
# 2)  设备和账号权重表  每天维护   SQL
# 3)  空设备临时guid表每天维护   SQL
# 4)  全局guid生成  且导入到DWD层  SQL