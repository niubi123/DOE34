#!/bin/bash
# 加载 app域的用户行为数据到原始层
DT=$(date  -d'-1 days'  +%Y-%m-%d)
if [ $1 ]
then   DT=$1
fi
hive -e "load data inpath '/doe/applogs/${DT}' into table doe34.ods_app_event_log partition (dt='${DT}') ;"
if [ $? -eq 0 ]
    then echo  "${DT}天的app_log数据入库ODS成功..." | mail -s 'ODS入库成功通知'  598196583@qq.com
    else echo  "${DT}天的app_log数据入库ODS失败..." | mail -s 'ODS入库失败通知'  598196583@qq.com
fi
