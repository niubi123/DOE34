#!/bin/bash
DATE=$(date  -d'-1 days'  +%Y-%m-%d)

function local_log_report(){
  #  判断文件夹下是有内容
  if [ $(ls /doe/data/logs/ | wc -l) -gt 0 ]
    then  echo "/doe/data/logs/ 有内容"
    # 遍历指定文件夹下的内容
     for logtype in $(ls /doe/data/logs/)
     do
         # 判断 是否是日志文件夹
        if  [ -d /doe/data/logs/${logtype} ]
        then
            # 上报各种日志类型的数据
            LOGTYPE=${logtype}
            echo "日志类型有: ${LOGTYPE}"
            CNT=$(cat /doe/data/logs/${logtype}/*${DATE}.log | wc -l)
            curl http://windows:8080/collectLogCount -X POST  -d"{\"hostName\":\"${HOSTNAME}\",\"logType\":\"${LOGTYPE}\" ,\"date\":\"${DATE}\" ,\"cnt\":\"${CNT}\"}" -H "Content-Type: application/json"
        else  echo "/doe/data/logs/${logtype} 是文件"
        fi
     done
    else echo "空的"
  fi
}

# 将每天的HDFS上收集到的不同日志类型的日志条数上报
function  hdfs_log_report(){
  CNT=$(hdfs  dfs  -text  /doe/${DATE}/applog/*  | wc -l)
   curl http://windows:8080/collectLogCount -X POST  -d"{\"hostName\":\"HDFS\",\"logType\":\"applog\" ,\"date\":\"${DATE}\" ,\"cnt\":\"${CNT}\"}" -H "Content-Type: application/json"
}
# 调用方法
local_log_report
# 调用方法
hdfs_log_report