show tables ;
-- 创建 原始层数据表
-- ods 数据原始层  贴源层
-- 库 , 层_数据域_主题_内容
-- 原始数据  字段的命名 不规范 和数据的属性对应  方便数据加载
-- 命名规范 doe.ods_app_event_log
create external  table doe34.ods_app_event_log(
 account string comment  '用户账号' ,
 appid string comment  '程序唯一标识' ,
 appversion string comment  '程序版本' ,
 carrier string comment  '网络运营商' ,
 deviceid string comment  '设备唯一标识 设备入网许可证' ,
 devicetype string comment  '设备类型' ,
 eventid string comment  '行为事件' ,
 ip string comment  'ip' ,
 latitude double comment  '维度' ,
 longitude double comment  '经度' ,
 nettype string comment  '网络类型' ,
 osname string comment  '系统名' ,
 osversion string comment  '系统版本' ,
 properties map<string,string>  comment  '对事件进行补充 比如 点击广告事件,广告id 广告类别...' ,
 releasechannel  string comment  '下载渠道' ,
 resolution string comment  '分辨率' ,
 sessionid  string comment  '会话ID' ,
 `timestamp` bigint  comment  '时间戳'
) comment  'app用户行为原始数据'
    partitioned by (dt string)
    row format serde 'org.apache.hive.hcatalog.data.JsonSerDe';

show  tables ;
desc formatted  ods_app_event_log ;
-- 手动加载指定日期的数据
load data  inpath '/doe/applogs/2022-10-25' into table doe34.ods_app_event_log partition (dt='2022-10-25') ;
select
    account ,
    eventid ,
    ip ,
    latitude ,
    longitude ,
    `timestamp` ,
    sessionid
from ods_app_event_log  where  trim(account) !=  ''  limit 100;

-- 查看表的分区
show partitions   doe34.ods_app_event_log ;
select count(1)  from  doe34.ods_app_event_log where dt = '2022-10-26'  limit  10;
