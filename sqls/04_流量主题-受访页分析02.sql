show tableS  ;
show databases  ;
use doe34  ;

set hive.vectorized.execution.enabled = false;
set hive.exec.mode.local.auto=true;
select
    t1.page_url,
    t1.guid,
    t1.session_id,
    t1.ref_url,
    t1.ref_type,
    lead(t1.ts , 1 , t2.end_ts) over(partition by  t1.session_id  order by  t1.ts)  - t1.ts  as  acc_long ,
    ts
from
    (select
         properties['url']  as  page_url ,
         guid   ,
         new_session as session_id ,
         properties['refUrl']  as  ref_url ,
         case
             when   properties['refUrl'] like  '/%'  then   '站内搜索'
             when   properties['refUrl'] like  'http/%'  then '外部来源'
             else  '其他'
             end   as  ref_type ,
         ts

     from doe34.dwd_app_event_log_dtl where  dt = '2022-10-26'  and  event_id ='pageView' )t1
        join
    doe34.dws_flow_acc_page_sess_agg  t2
    on  t1.session_id = t2.session_id  and  t2.dt='2022-10-26';
--  页面粒度的表

create  table  if  not exists   doe34.dws_flow_acc_page_dtl(
                                                               page_url  string  comment '受访页url' ,
                                                               guid  bigint comment   '用户' ,
                                                               session_id  string comment  '页面所属的会话' ,
                                                               ref_url string  comment   '页面的来源地址'  ,
                                                               ref_type string  comment    '页面来源类型' ,
                                                               acc_long  bigint  comment   '页面访问时长' ,
                                                               ts  bigint   comment   '页面打开时间'
)
    partitioned by (dt  string)
    stored as  orc
    tblproperties ('orc.compress'='snappy') ;
insert into table doe34.dws_flow_acc_page_dtl  partition (dt= '2022-10-26')
select
    t1.page_url,
    t1.guid,
    t1.session_id,
    t1.ref_url,
    t1.ref_type,
    lead(t1.ts , 1 , t2.end_ts) over(partition by  t1.session_id  order by  t1.ts)  - t1.ts  as  acc_long ,
    ts
from
    (select
         properties['url']  as  page_url ,
         guid   ,
         new_session as session_id ,
         properties['refUrl']  as  ref_url ,
         case
             when     properties['refUrl'] like  '/%'  then   '站内搜索'
             when     properties['refUrl'] like  'http/%'  then '外部来源'
             else  '其他'
             end   as  ref_type ,
         ts

     from doe34.dwd_app_event_log_dtl where  dt = '2022-10-26'  and  event_id ='pageView' )t1
        join
    (
        select session_id , end_ts from   doe34.dws_flow_acc_page_sess_agg  where dt = '2022-10-26'

    )t2
    on  t1.session_id = t2.session_id ;





------------------------------受访页 单一维度报表

create  table  doe34.ads_flow_acc_page_report_d(
                                                   page_url  string ,
                                                   pv  int ,
                                                   uv  int  ,
                                                   folow_pv  int ,
                                                   exit_cnt int  ,
                                                   enter_cnt int ,
                                                   avg_acc_long  bigint

) partitioned by (dt string)
    stored as  orc
    tblproperties ('orc.compress'='snappy') ;

--
WITH x1 AS (
    SELECT
        page_url,
        COUNT(1) AS pv,
        COUNT(DISTINCT guid) AS uv,
        AVG(acc_long) AS avg_acc_long
    FROM
        doe34.dws_flow_acc_page_dtl
    WHERE
            dt = '2022-10-26'
    GROUP BY
        page_url
),
     x2 AS (
         --  页面作为引用页面的次数
         SELECT
             ref_url,
             COUNT(1) AS folow_pv
         FROM
             doe34.dws_flow_acc_page_dtl
         WHERE
                 dt = '2022-10-26'
         GROUP BY
             ref_url
     ),
     x3 AS (
         -- 页面作为退出页的次数
         SELECT
             exit_page,
             COUNT(1) AS exit_cnt
         FROM
             doe34.dws_flow_acc_page_sess_agg
         WHERE
                 dt = '2022-10-26'
         GROUP BY
             exit_page
     ),
     x4 AS (
         -- 页面作为入口页的次数
         SELECT
             enter_page,
             COUNT(1) AS enter_cnt
         FROM
             doe34.dws_flow_acc_page_sess_agg
         WHERE
                 dt = '2022-10-26'
         GROUP BY
             enter_page
     )
INSERT INTO
    TABLE doe34.ads_flow_acc_page_report_d PARTITION(dt = '2022-10-26')
SELECT
    x1.page_url AS page_url,
    x1.pv AS pv,
    x1.uv AS uv,
    nvl(x2.folow_pv, 0) AS folow_pv ,
    nvl(x3.exit_cnt, 0) AS exit_cnt,
    nvl(x4.enter_cnt, 0) AS enter_cnt,
    x1.avg_acc_long AS avg_acc_long
FROM
    x1
        LEFT JOIN x2 ON x1.page_url = x2.ref_url
        LEFT JOIN x3 ON x1.page_url = x3.exit_page
        LEFT JOIN x4 ON x1.page_url = x4.enter_page;

-- 查询结果  数据探测 (过滤)
-- [测试集群 , 测试数据 , 执行 观测结果]
select * from doe34.ads_flow_acc_page_report_d  where dt = '2022-10-26'  ;

-- group  by

-- 新老属性下 页面的指标

-- 不同地域下 页面的指标

-- 不同地域下  不同新老属性   页面的指标
--中心事实表  页面明细
-- 关联 会话粒度表[是否是退出页 入口页]   用户粒度表[省市区 新老]
desc doe34.dws_flow_acc_page_dtl ;
desc doe34.dws_flow_acc_page_sess_agg ;
desc doe34.dws_flow_acc_page_user_agg ;

create  table  doe34.ads_flow_acc_page_report_cube_d(
                                                        page_url  string ,
                                                        provnce string  ,
                                                        is_new int ,
                                                        pv  int ,
                                                        uv  int  ,
                                                        folow_pv  int ,
                                                        exit_cnt int  ,
                                                        enter_cnt int ,
                                                        avg_acc_long  bigint

) partitioned by (dt string)
    stored as  orc
    tblproperties ('orc.compress'='snappy') ;



select
    t1.page_url,
    t1.province,
    t1.is_new,
    t1.pv,
    t1.uv,
    t2.folow_pv as  folow_pv ,
    t3.exit_cnt as exit_cnt ,
    t4.enter_cnt as enter_cnt,
    t1.avg_acc_long
from
    (select
         page_url ,
         province ,
         is_new ,
         count(1) as  pv ,
         count(distinct  guid) as uv ,
         avg(acc_long) as  avg_acc_long
     from
         (select
              x1.page_url,
              x1.guid,
              x1.session_id,
              x1.acc_long,
              x2.is_new ,
              x2.province
          from
              (select
                   page_url , guid ,session_id, acc_long
               from doe34.dws_flow_acc_page_dtl where  dt = '2022-10-26')x1

                  join

              (select guid , is_new , province ,city ,area
               from doe34.dws_flow_acc_page_user_agg where  dt = '2022-10-26')x2
              on x1.guid = x2.guid ) o
     group by  page_url , province , is_new)t1

        left join
    (
        select
            ref_url ,
            count(1) as  folow_pv
        from
            doe34.dws_flow_acc_page_dtl where dt = '2022-10-26'
        group by  ref_url
    )t2
        left join
    (
        select
            exit_page ,
            count(1) as exit_cnt
        from doe34.dws_flow_acc_page_sess_agg where  dt = '2022-10-26'
        group by  exit_page

    )t3
        left join
    (
        select
            enter_page ,
            count(1) as enter_cnt
        from doe34.dws_flow_acc_page_sess_agg where  dt = '2022-10-26'
        group by  enter_page
    )t4
    on   t1.page_url = t2.ref_url
        and  t1.page_url = t3.exit_page
        and  t1.page_url = t4.enter_page ;

-------------------------构建宽表模型完成受访页多维分析-----------------------------
-- 宽表的目的是为了最终的多维报表
-- 宽表  计算出报表  逻辑尽量简单  is_new  ref_type  acc_long (在创建宽表时 也有计算的聚合逻辑)  folow_pv(代码:树)

-- page_url , guid , session_id , is_new , province , city , area , ref_url , ref_type , is_exit , is_enter , ts , acc_long , folow_pv
/*A,1,seesion01                                                      /index                   0         1       2
B,1,seesion01                                                      /A                   0         0       5
C,1,seesion01                                                      /A                   0         0       9
X,1,seesion01                                                      /C                   1         0       19
J,1,seesion01                                                      /C
A,2,seesion02                                                      /index                   0         1
B,2,seesion02                                                      /index                   0         0
C,2,seesion02                                                      /index                   0         0
X,2,seesion02                                                      /index                   1         0
A,3,seesion03                                                      /index                   0         1
B,3,seesion03                                                      /index                   1         0
C,3,seesion04                                                      /index                   0         1
X,3,seesion04                                                      /index  */                 1         0

-- 基本要求  满足 多维度受访页分析的所有分析需求
create  table  if  not exists  doe34.dws_flow_acc_page_wide(
                                                               page_url  string ,
                                                               guid bigint ,
                                                               session_id  string ,
                                                               ref_url string ,
                                                               ref_type  string ,
                                                               is_new int  ,
                                                               follow_pv  int , -- 下游贡献量   使用树
                                                               is_exit_page int , --是否是退出页  1 是  0 不是
                                                               is_enter_page int  ,
                                                               acc_long  bigint ,
                                                               province  string  ,
                                                               city  string  ,
                                                               area  string ,
                                                               ts  bigint
)
    partitioned by (dt string)
    stored as  orc
    TBLPROPERTIES ('orc.compress'='snappy') ;

--  有了宽表以后  可以多维度分析受访页需求  , 不再需要多张表的关联  提升效率
-- 构建立方体模型   预计算    Kylin
/*
 但是 需要写很多sql  sql中的计算逻辑是一样
     with  cube语法
group  by  is_new , page_url
group  by  ref_type , page_url
group  by  province , city  , page_url
group  by  province , city , is_new , page_url
*/



create  table  tb_test_cube(
                               guid bigint , -- 没有唯一保证
                               account string  ,
                               gender string ,
                               province string  ,
                               city string  ,
                               area string

)
    row format delimited  fields terminated by  ','
;
1,guanyu,M,河南省,郑州市,驻马店
2,宝强,M,河南省,郑州市,南城
3,韩红,M,河南省,洛阳,西城
4,亚东,M,河南省,洛阳,西城

1       guanyu  M       河南省  郑州市  驻马店
2       宝强    M       河南省  郑州市  南城
3       韩红    M       河南省  洛阳    西城
4       亚东    M       河南省  洛阳    西城

select province ,   cnt from
    (select
         province
          ,city
          , area ,
         count(1) cnt
     from
         tb_test_cube
     group by  province ,city , area
     with cube)t
where  province is   null  and city is not null and area is null
;


/*NULL    NULL    NULL    4

NULL    NULL    南城    1
NULL    NULL    西城    2
NULL    NULL    驻马店  1

河南省  NULL    NULL    4

NULL    洛阳    NULL    2
NULL    郑州市  NULL    2

NULL    洛阳    西城    2
NULL    郑州市  南城    1
NULL    郑州市  驻马店  1

河南省  NULL    南城    1
河南省  NULL    西城    2
河南省  NULL    驻马店  1

河南省  洛阳    NULL    2
河南省  郑州市  NULL    2

河南省  洛阳    西城    2
河南省  郑州市  南城    1
河南省  郑州市  驻马店  1*/

select
    province , city ,area , count(1)
from tb_test_cube
group by province , city ,area
    grouping sets
    (
         province ,
    (province,city) ,
         area
    )
;
select
    province , city ,area , count(1)
from tb_test_cube
group by province , city ,area
with  rollup ;
--  不支持
select
    province , city ,area , count(1)
from tb_test_cube
group by province , city ,area
-- with total ;
/*
null  null  null     4
河南省  洛阳    西城    2
河南省  郑州市  南城    1
河南省  郑州市  驻马店  1
*/


select
    ref_type , is_new , province ,city , area , page_url ,
    count(1) pv ,
    count(distinct  guid) uv
from
    doe34.dws_flow_acc_page_wide
group by ref_type , is_new , province ,city , area , page_url
    grouping sets (
    (ref_type,is_new ,page_url) ,
    (ref_type,province,page_url) ,
    (ref_type ,is_new ,province ,page_url)
    );

-- spark-sql   SQL  语法有细微的差距
-- HQL         SQL  语法有细微的差距

-- 来源+新老
-- 新老+省
-- 来源 +新老 +省

-- 1统计各性别的总人数
select
    gender ,
    count(distinct  guid)
from tb_test_cube
group by gender ;
-- 2统计各省份的总人数
select
    province ,
    count(distinct  guid)
from tb_test_cube
group by province ;

-- 3统计各省份下各性别的总人数
select
    province ,gender ,
    count(distinct  guid)
from tb_test_cube
group by province  , gender ;

-- 4统计各省份下城市的总人数
select
    province ,city ,
    count(distinct  guid)
from tb_test_cube
group by province  , city ;

-- 5统计各省份下城市下各性别的总人数
select
    province ,city ,gender ,
    count(distinct  guid)
from tb_test_cube
group by province  , city , gender  ;

-----------------------------------------
select
    province ,city ,gender ,
    count(distinct  guid)
from tb_test_cube
group by province  , city , gender
with cube;
------------------------------------------






-- 页面粒度
-- 用户粒度

-- 基本要求  满足 多维度受访页分析的所有分析需求
create  table  if  not exists  doe34.dws_flow_acc_page_wide(
                                                               page_url  string ,  --  加载    DWD  数据
                                                               guid bigint ,
                                                               session_id  string ,
                                                               ref_url string ,
                                                               ref_type  string , -- 根据规则匹配类型  parse_url
                                                               is_new int  , -- X
                                                               follow_pv  int , -- 下游贡献量   使用树   优化
                                                               is_exit_page int , --是否是退出页  1 是  0 不是
                                                               is_enter_page int  ,
                                                               acc_long  bigint ,
                                                               province  string  ,
                                                               city  string  ,
                                                               area  string ,
                                                               ts  bigint
)
    partitioned by (dt string)
    stored as  orc
    TBLPROPERTIES ('orc.compress'='snappy') ;

show tables  in doe34 ;

desc doe34.dws_flow_acc_page_sess_agg ;
desc  doe34.dws_flow_acc_page_dtl ;
desc  doe34.dws_flow_acc_page_user_agg


--------------------------------测试数据

    a,session01,101
b,session01,201
f,session01,301
j,session01,401
k,session01,602

create table tb_test_exit(
                             page_url  string,
                             session_id  string,
                             ts bigint
)
    row format delimited  fields terminated by  ',' ;

load data  local  inpath  '/data/a.exit' into   table tb_test_exit ;
select * from tb_test_exit ;


select
    *  ,
    if ( row_number() over (partition by session_id order by ts)  == 1 , 1, 0) as  is_enter ,
    if ( row_number() over (partition by session_id order by ts desc )  == 1 , 1, 0) as  is_exit
from tb_test_exit;


--

select
    * ,
    if(page_url == split(max(ts||'-'||page_url)  over (partition by session_id) ,'-')[1] , 1, 0)  as  is_exit ,
    if(page_url == split(min(ts||'-'||page_url)  over (partition by session_id) ,'-')[1] , 1, 0)  as  is_enter
from tb_test_exit;





page_dtl
  /**
     * root      page_dtl
     * |-- page_url: string (nullable = true)
     * |-- guid: long (nullable = false)
     * |-- session_id: string (nullable = true)
     * |-- ref_url: string (nullable = true)
     * |-- ref_type: string (nullable = true)
     * |-- follow_pv: integer (nullable = false)
     * |-- acc_long: long (nullable = false)
     * |-- ts: long (nullable = false)
     */

select
    t1.page_url ,
    t1.guid ,
    t1.session_id ,
    t1.ref_url ,
    t1.ref_type ,
    t2.is_new ,
    t1.follow_pv ,
    t1. is_exit ,
    t1.is_enter ,
    t1.acc_long ,
    t2.province ,
    t2,city ,
    t2.area ,
    t1.ts

from
    ( select
          page_url ,
          guid ,
          session_id ,
          ref_url ,
          ref_type ,
          follow_pv ,
          acc_long ,
          if (row_number() over (partition by session_id order by ts desc )  == 1 , 1, 0) as  is_exit ,
          if (row_number() over (partition by session_id order by ts)  == 1 , 1, 0) as  is_enter ,
          ts
      from page_dtl
    ) t1
        join
    (select
         guid ,is_new , province , city , area
     from
         doe34.dws_flow_acc_page_user_agg  where  dt = '2022-10-26') t2
    on t1.guid = t2.guid

select
    *
from
    doe34.dws_flow_acc_page_wide where  dt = '2022-10-26' ;
-- 受访页的  pv  uv  follow_pv exit_cnt , avg_acc_long
select
    page_url ,
    count(1) pv,
    count(distinct  guid) uv  ,
    sum(follow_pv) as follow_pv ,
    sum(is_exit) as  exit_cnt ,
    avg(acc_long) as avg_acc_long
from
    doe34.dws_flow_acc_page_wide where  dt = '2022-10-26'
group by  page_url
;
-- 新老属性  受访页的  pv  uv  follow_pv exit_cnt , avg_acc_long
select
    page_url ,
    is_new ,
    count(1) pv,
    count(distinct  guid) uv  ,
    sum(follow_pv) as follow_pv ,
    sum(is_exit) as  exit_cnt ,
    avg(acc_long) as avg_acc_long
from
    doe34.dws_flow_acc_page_wide where  dt = '2022-10-26'
group by  page_url ,is_new
;
--不同来源  新老属性  受访页的  pv  uv  follow_pv exit_cnt , avg_acc_long
select
    page_url ,
    ref_type ,
    is_new ,
    count(1) pv,
    count(distinct  guid) uv  ,
    sum(follow_pv) as follow_pv ,
    sum(is_exit) as  exit_cnt ,
    avg(acc_long) as avg_acc_long
from
    doe34.dws_flow_acc_page_wide where  dt = '2022-10-26'
group by  page_url  , ref_type,is_new
;
-- 高阶语法
--不同来源  新老属性 省  受访页的  pv  uv  follow_pv exit_cnt , avg_acc_long
select
    page_url ,
    ref_type ,
    is_new ,
    province,
    count(1) pv,
    count(distinct  guid) uv  ,
    sum(follow_pv) as follow_pv ,
    sum(is_exit) as  exit_cnt ,
    avg(acc_long) as avg_acc_long
from
    doe34.dws_flow_acc_page_wide where  dt = '2022-10-26'
group by  page_url  , ref_type,is_new  , province
;
------------------------------
select
    page_url ,
    ref_type ,
    is_new ,
    province,
    count(1) pv,
    count(distinct  guid) uv  ,
    sum(follow_pv) as follow_pv ,
    sum(is_exit) as  exit_cnt ,
    avg(acc_long) as avg_acc_long
from
    doe34.dws_flow_acc_page_wide where  dt = '2022-10-26'
group by  page_url  , ref_type,is_new  , province
    grouping sets (
    (page_url) ,
    (page_url ,is_new) ,  -- √
    (page_url ,ref_type, is_new)  ,
    (page_url , ref_type , is_new , province)
    )
;
----> 构建所谓分析立方体  [结果保存在表中]  doe34.dws_flow_acc_page_cube

/*select
 ... ...
from doe34.dws_flow_acc_page_cube  where  dt = '2022-10-26'
and  page_url is not null  and is_new is not null  and  ref_type is null  and province is  null ;*/
-- and  page_url is not null  and is_new is not null  and  ref_type is not  null  and province is  null ;