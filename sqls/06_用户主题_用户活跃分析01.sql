-- 地理位置拉链表练习
-- 规定 :当天所在的最后一个地点是 当日的地理位置
set hive.exec.mode.local.auto=true;
-- 2022-10-25的地理位置拉链表
1,2022-10-01,江西省,南昌市,革命区,2022-10-01,2022-10-05
1,2022-10-01,江苏省,南京市,下关区,2022-10-06,9999-12-31
2,2022-10-01,黑龙江,哈尔滨,道外区,2022-10-01,9999-12-31
3,2022-10-01,湖南省,长沙市,岳麓区,2022-10-01,9999-12-31
4,2022-10-01,湖南省,长沙市,芙蓉区,2022-10-01,9999-12-31
-- 地理位置拉链表
create table if not exists doe34.test_uac_area_range
(
    guid        bigint,
    register_dt string,
    province    string,
    city        string,
    area        string,
    start_dt    string,
    end_dt      string
)
    partitioned by (dt string)
    row format delimited fields terminated by ',';
load data local inpath '/data/area.rng' into table doe34.test_uac_area_range partition (dt = '2022-10-25');

-- 2022-10-26的用户地域信息
1,2022-10-01,江苏省,南京市,雨花区,1
1,2022-10-01,江苏省,南京市,雨花区,2
1,2022-10-01,江苏省,南京市,鼓楼区,3
1,2022-10-01,江苏省,南京市,鼓楼区,4
2,2022-10-01,黑龙江,哈尔滨,道里区,1
4,2022-10-01,湖南省,长沙市,芙蓉区,1
5,2022-10-26,上海市,上海市,闵行区,1
-- 用户的地理位置轨迹表
create table if not exists doe34.test_uac_area_dtl
(
    guid        bigint,
    register_dt string,
    province    string,
    city        string,
    area        string,
    ts          bigint
)
    partitioned by (dt string)
    row format delimited fields terminated by ',';
load data local inpath '/data/area.csv' into table doe34.test_uac_area_dtl partition (dt = '2022-10-26');

select *
from doe34.test_uac_area_range
where dt = '2022-10-25';

select *
from doe34.test_uac_area_dtl
where dt = '2022-10-26';

-- 所有的历史状态数据  闭合的数据 不会变化
1,2022-10-01,江西省,南昌市,革命区,2022-10-01,2022-10-05
-- 能关联到今天的数据  且地理位置发生了变化  :  闭合  +  新增
1,2022-10-01,江苏省,南京市,下关区,2022-10-06,2022-10-25
1,2022-10-01,江苏省,南京市,鼓楼区,2022-10-26,9999-12-31
2,2022-10-01,黑龙江,哈尔滨,道外区,2022-10-01,2022-10-25
2,2022-10-01,黑龙江,哈尔滨,道里区,2022-10-26,9999-12-31
--今天没有出现的数据 封死
3,2022-10-01,湖南省,长沙市,岳麓区,2022-10-01,2022-10-25
-- 今天出现了  但是地理位置没有变化 , 数据不变
4,2022-10-01,湖南省,长沙市,芙蓉区,2022-10-01,9999-12-31
-- 新增的用户信息
5,2022-10-26,上海市,上海市,闵行区,2022-10-26,9999-12-31

分析 :

-- 没有闭合的数据
1,2022-10-01,江苏省,南京市,下关区,2022-10-06,9999-12-31
2,2022-10-01,黑龙江,哈尔滨,道外区,2022-10-01,9999-12-31
3,2022-10-01,湖南省,长沙市,岳麓区,2022-10-01,9999-12-31
4,2022-10-01,湖南省,长沙市,芙蓉区,2022-10-01,9999-12-31

1,2022-10-01,江苏省,南京市,鼓楼区,2
1,2022-10-01,江苏省,南京市,鼓楼区,3
1,2022-10-01,江苏省,南京市,鼓楼区,4
2,2022-10-01,黑龙江,哈尔滨,道里区,1
4,2022-10-01,湖南省,长沙市,芙蓉区,1
5,2022-10-26,上海市,上海市,闵行区,1

-- 1 完全闭合数据

with t1 as (select guid,
                   register_dt,
                   province,
                   city,
                   area,
                   start_dt,
                   end_dt,
                   dt
            from doe34.test_uac_area_range
            where dt = '2022-10-25'
              and end_dt = '9999-12-31'),
     t2 as (
         -- 用户的最新的地理位置信息
         select guid, register_dt, province, city, area, ts
         from (select guid,
                      register_dt,
                      province,
                      city,
                      area,
                      ts,
                      row_number() over (partition by guid order by ts desc) as rn
               from doe34.test_uac_area_dtl
               where dt = '2022-10-26'
              ) t
         where rn = 1
     )

insert
into table doe34.test_uac_area_range partition (dt = '2022-10-26')

select guid,
       register_dt,
       province,
       city,
       area,
       start_dt,
       end_dt
from doe34.test_uac_area_range
where dt = '2022-10-25'
  and end_dt != '9999-12-31'
union
-- 只关闭或者不变
select t1.guid,
       t1.register_dt,
       t1.province,
       t1.city,
       t1.area,
       t1.start_dt,
       if(t2.guid is not null and t1.province || t1.city || t1.area != t2.province || t2.city || t2.area, '2022-10-25',
          t1.end_dt) as end_dt
from t1
         left join
     t2
     on t1.guid = t2.guid

-- 数据新增  新的用户 , 地理位置变化的数据
union

select t2.guid,
       nvl(t1.register_dt, t2.register_dt) as register_dt,
       t2.province,
       t2.city,
       t2.area,
       '2022-10-26'                        as start_dt,
       '9999-12-31'                        as end_dt
from t2
         left join
     t1
     on t1.guid = t2.guid
where (t1.province || t1.city || t1.area != t2.province || t2.city || t2.area or t1.guid is null)
;

-- 10月25号  各个省份的日活用户
with uac as (
    select *
    from doe34.test_dws_app_uac_range
    where '2022-10-25'
              between start_dt and end_dt
),
     area as (
         select *
         from doe34.test_uac_area_range
         where dt = '2022-10-26'
           and '2022-10-25' between start_dt and end_dt
     )
select area.province,
       area.city,
       count(uac.guid) as cnt
from uac
         join area
              on uac.guid = area.guid
group by area.province, area.city
    grouping sets (
    ( province),
    ( province, city)
    )
;

-- 10月25号  各个省份的日活用户
-- 10月26号  各个省份的日活用户


-- 最近7天  各个省份的平均活跃天数
-- 用户的活跃区间表
-- 用户活动轨迹表
-- 规定计算日用户所在的地理位置就是 计算的地理位置
--------------------------------------
第一步 : 计算最近7天 每个用户的总活跃天数
第二步:  获取计算日每个人的地理位置信息
第三步:  用户关联地理位置
第四步:  分组聚合
-------------------------------------
订单拉链表练习  , 由于订单的状态是一个缓慢变化的 , 状态范围


2022-10-25 的拉链数据
oid,status,start_dt,end_dt
1,未支付,2022-10-20,2022-10-22
1,已支付,2022-10-23,2022-10-24
1,已发货,2022-10-25,9999-12-31
2,待支付,2022-10-25,9999-12-31
3,已支付,2022-10-25,9999-12-31
4,已完成,2022-10-25,9999-12-31
5,已发货,2022-10-25,9999-12-31

2022-10-26 日的 订单增量数据   (状态更新的订单  / 新增的订单)
-- 数据库中当前的订单数据
1,已收货  修改时间 对
2,已支付  修改时间 对
3,已支付  修改时间
4,已完成  修改时间
5,已完成  修改时间 对
6,待支付  修改时间 对

-- 2922-10-26变化的增量数据
1,已收货   对
2,已支付   对
5,已完成   对
6,待支付   对

create table if not exists doe34.test_orders_range
(
    oid      bigint,
    status   string,
    start_dt string,
    end_dt   string
)
    partitioned by (dt string)
    row format delimited fields terminated by ',';
load data local inpath '/data/orders.rng' into table doe34.test_orders_range partition (dt = '2022-10-25');

create table if not exists doe34.test_orders_dtl
(
    oid    bigint,
    status string
)
    partitioned by (dt string)
    row format delimited fields terminated by ',';
load data local inpath '/data/orders.csv' into table doe34.test_orders_dtl partition (dt = '2022-10-26');

select *
from doe34.test_orders_range
where dt = '2022-10-25';


select *
from doe34.test_orders_dtl
where dt = '2022-10-26';
区间表  left join  最新的状态表
能关联上 且 end_dt=9999  封闭四  其他不变

1,未支付,2022-10-20,2022-10-22
1,已支付,2022-10-23,2022-10-24
1,已发货,2022-10-25,9999-12-31
2,待支付,2022-10-25,9999-12-31
3,已支付,2022-10-25,9999-12-31
4,已完成,2022-10-25,9999-12-31
5,已发货,2022-10-25,9999-12-31


1,已收货
2,已支付
5,已完成
6,待支付


with x1 as (
    select oid,
           status,
           start_dt,
           end_dt
    from doe34.test_orders_range
    where dt = '2022-10-25'
),
     x2 as (
         select oid, status
         from doe34.test_orders_dtl
         where dt = '2022-10-26'
     )

select x1.oid,
       x1.status,
       x1.start_dt,
       if(x2.oid is not null and x1.end_dt = '9999-12-31', '2022-10-25', x1.end_dt) as end_dt
from x1
         left join x2
                   on x1.oid = x2.oid
union
select x2.oid,
       x2.status,
       '2022-10-26' as start_dt,
       '9999-12-31' as end_dt
from x2

/*
   select
        oid ,status , start_dt , end_dt
        from doe34.test_orders_range where dt = '2022-10-25' and  end_dt !='9999-12-31'
union

   select
        oid ,status , start_dt , end_dt
        from doe34.test_orders_range where dt = '2022-10-25' and  end_dt ='9999-12-31'
left join
  select oid , status
        from doe34.test_orders_dtl where  dt = '2022-10-26'

union
 */

-- 一个订单保持一个最新的状态


    封闭的数据
with x as (
    select oid,
           status,
           start_dt,
           end_dt
    from doe34.test_orders_range
    where dt = '2022-10-25'
    union
    select oid,
           status,
           '2022-10-26' as start_dt,
           '9999-12-31' as end_dt
    from doe34.test_orders_dtl
    where dt = '2022-10-26'
)
select oid,
       status,
       start_dt,
       if(row_number() over (partition by oid order by start_dt desc ) = 2, '2022-10-25', end_dt) as end_dt
from x;


/*

1,未支付,2022-10-20,2022-10-22
1,已支付,2022-10-23,2022-10-24
1,已发货,2022-10-25,2022-10-25
2,待支付,2022-10-25,2022-10-25
3,已支付,2022-10-25,9999-12-31
4,已完成,2022-10-25,9999-12-31
5,已发货,2022-10-25,2022-10-25
1,已收货,2022-10-26,9999-12-31
2,已支付,2022-10-26,9999-12-31
5,已完成,2022-10-26,9999-12-31
6,待支付,2022-10-26,9999-12-31*/

DWD: 明细层
DWS: uac_agg

--  基于 用户活跃轻度聚合表  维护  用户访问的拉链表
create table if not exists doe34.dws_app_uac_range
( -- 计算来源 用户活跃轻度聚合表
    guid     bigint,
    start_dt string,
    end_dt   string
)
    partitioned by (dt string)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');

--  用户活跃轻度聚合表
create table if not exists doe34.dws_app_uac_area_range
( -- 计算来源 用户活跃轻度聚合表
    guid     bigint,
    province String,
    city     string,
    area     string,
    start_dt string,
    end_dt   string
)
    partitioned by (dt string)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');

desc doe34.dws_flow_acc_page_user_agg;
-- 用户的地理位置信息 最后一个地点   split(max(ts||'-'||province|), '-')[1]

-- 用户主题   活跃分析报表

-- 最近7天的   总访客数  平均访问天数   总访问天数
-- 最近14天的  总访客数  平均访问天数   总访问天数
-- 最近30天的  总访客数  平均访问天数   总访问天数

create table if not exists doe34.ads_app_uac_rept_7
(
    comput_dt     string,
    total_uac_cnt int,
    avg_acc_days  double,
    acc_days      int
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');

create table if not exists doe34.ads_app_uac_rept_14
(
    comput_dt     string,
    total_uac_cnt int,
    avg_acc_days  double,
    acc_days      int
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');
create table if not exists doe34.ads_app_uac_rept_30
(
    comput_dt     string,
    total_uac_cnt int,
    avg_acc_days  double,
    acc_days      int
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');


create table if not exists doe34.ads_app_uac_frequency
(
    week_start_dt  string,
    total_uac_cnt  bigint,
    uac_cnt_1_days int,
    uac_cnt_2_days int,
    uac_cnt_3_days int,
    uac_cnt_4_days int,
    uac_cnt_5_days int,
    uac_cnt_6_days int,
    uac_cnt_7_days int
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy') ;

-- 用户活跃  多维分析  添加了省市区

create table if not exists doe34.ads_app_uac_area_cube_rept_7days
(
    comput_dt     string,
    province string ,
    city  string ,
    total_uac_cnt int,
    avg_acc_days  double,
    acc_days      int
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');


CREATE TABLE ads.mall_app_user_act_01(
                                         guid                bigint
    ,uac_days           bigint
    ,max_uac_days       bigint
    ,max_silence_days   bigint
)
    partitioned by (dt string)
    stored as orc
    tblproperties('orc.compress'='snappy')
;


select  * from
    doe34.test_dws_app_uac_range   ;
load data  local  inpath  '/data/uac.rng' into  table  doe34.test_dws_app_uac_range   ;

select
    '2022-10-27' as comput_dt ,
    datediff('2022-10-27' , first_login) as  lc ,
    count(1) as  cnt
from doe34.test_dws_app_uac_range
where  end_dt = '9999-12-31'
group by   datediff('2022-10-27' , first_login)
;

归因
