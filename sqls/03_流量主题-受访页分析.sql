--  流量主题下的    受访页分析需求 --> 每个页面的   pv , uv , 下游贡献量 , 退出页次数 , 入口页次数 , 平均停留时长
--  入口页次数 根据现有的需求衍生出新的需求
--  受访页url , pv , uv , 下游贡献量 , 退出页次数 , 入口页次数 , 平均停留时长
--  度量/指标: 一个可以通过计算量化的统计需求(比如 金额 ,次数 ,时长,人数,占比... ...)    pv , uv , 下游贡献量 , 退出页次数 , 入口页次数 , 平均停留时长
--  维度: 审视 审查  统计数据的角度/粒度属性 比如 地域下的   不同来源下的  新老访客下的
--  一个需求  由维度和度量组成
--   确认主题 (流量主题)  确定维度  确认度量 (同名不同义)   确认计算逻辑

-- 第一步:  确定维度  :  新老属性->  地域信息 -> 页面来源 ->  page_url  中心事实(统计的核心计算逻辑粒度)   ;  隐形的日期 默认是T日  [计算的数据所在的日期]
-- 第二步:  确认度量 +  计算逻辑
-- 维度构建方式  地域信息:表字段   新老属性:计算   页面来源:计算  栏目:手动构建/爬虫   会员等级:关联业务表




-- pv: 页面的访问次数   count(1)   group by page_url
-- uv:    count(distinct  guid)  group by page_url
-- flow_pv    的时候计算需要           count(1)   group by ref_url               ref_url这个属性 字段   建模时确定表结构的重要依据
-- 退出页次数   计算出每个会话的退出页面
-- 入口页次数   计算出每个会话的入口页面
-- 每个页面的平均时长   每个页面每次打开的时长                                                 ts

/*                       , is_new , ref_url
index,session01,guid01,1         , /搜索
page1,session01,guid01,1         , index
page1,session01,guid01,1         , index
page1,session0x,guid01,1         , index


page1,session02,guid02,1
page2,session02,guid02,1
page3,session02,guid02,1
page4,session02,guid02,1
page5,session02,guid02,1
page4,session02,guid02,1  , index
page1,session03,guid06,1
page2,session03,guid06,1
page3,session03,guid06,1
page4,session03,guid06,1
page5,session03,guid06,1
page4,session03,guid06,1  , index

page1,session04,guid09,1
page2,session04,guid09,1
page3,session04,guid09,1
page4,session04,guid09,1
page5,session04,guid09,1
page6,session04,guid09,1  , index



page2,session03,guid03,0         , page1
page3,session04,guid04,1         , page1
page2,session05,guid05,0         , index
page3,session06,guid06,1         , page2
page3,session07,guid08,0         , page2

==> 页面
                page1,2
                page2,2
                page3,3

新老访客下的每个页面的 uv数*/

--  受访页分析   维度构建示例
-- 可能按照  时间  地域  页面来源类型  新老属性 分析统计页面的  pv  uv follow_pv  exit_cnt  enter_cnt  acc_long
-- DWD中只有下面的字段
_______________________________________________________
|guid|page_url|session_id|ref_url|ts|province|city|area| is_enter_page , is_exit_apge , is_new
1001,A,session01,index,101,山东省,济南市,历城区 ,             1           ,     0         ,   0
1001,B,session01,A,102,山东省,济南市,历城区                   0           ,     0         ,   0
1001,C,session01,B,103,山东省,济南市,历城区                   0           ,     1         ,   0
1002,A,session02,index,101,山东省,济南市,历城区 ,             1           ,     0          ,  1
1002,C,session02,A,103,山东省,济南市,历城区                   0           ,     0          ,  1
1002,A,session02,C,104,山东省,济南市,历城区                   0           ,     0          ,  1
1002,E,session02,C,105,山东省,济南市,历城区
1003,E,session04,C,105,山东省,济南市,历城区                       0           ,     1          ,  1
1004,E,session05,C,105,山东省,济南市,历城区
--  还需要     新老属性
--            页面来源类型
--            是否是退出页  是否是入口页  是否是跳出会话

--##############
-- 受访页统计分析中:   需求决定 , 有些维度或者是字段  ,需要提前计算  分别计算出结果字段  结果字段存储
-- 需要计算的字段存储在哪 ?? 根据计算的逻辑和计算的粒度存储在不同的表中

--  表一   用户粒度表   guid , is_new, 地理位置信息
--  表二   页面粒度表   page ,guid , session_id, ref_url , ref_type
--  表三   会话粒度     session_id, guid , start_ts,  end_ts , enter_page, exit_page , is_exit_session

-- 确定维度和度量     维度和度量在统计时    有些需求提前计算构建     多个构建的字段  逻辑和粒度不一致  分别构建
-- 将构建的多个       字段存储在不同的粒度表中   页面    用户    会话
-- 需求不能直接通过DWD统计得到 -->  提前构建字段 -->  构建表 -->  统计

/*  新老属性  用户  _is_new
需求1  :  新老访客下的pv uv
需求2  :  不同地域下的新老访客下的pv uv
需求3  :  不同页面来源下的新老访客下的pv uv
*/

--------------------------------------------会话粒度表------------------------------
-- 命名规范 一般大公司内部缩写规范 对照缩写单词表   acc: access  sess:session  agg:aggregate
-- 使用全名  缩写   维护缩写字典

drop  table if exists  doe34.dws_flow_acc_page_sess_agg ;
create  table  if not exists  doe34.dws_flow_acc_page_sess_agg(
    session_id string ,
    guid bigint  ,
    start_ts bigint  comment  '会话起始时间',
    end_ts bigint comment  '会话结束时间',
    enter_page string comment  '会话的入口页  第一个页面',
    exit_page string comment '会话的退出页  最后一个页面',
    is_jump_out int comment '是否是跳出会话 一个会话只打开一个页面 是:1   0', --  一个会话只打开一个页面
    session_pv int  comment '会话内部的页面数'
)
partitioned by (dt string)
stored as orc
tblproperties ('orc.compress'='snappy') ;
-- 维护表内容
-- 计算来源  dwd
se01,guid01,1,click
se01,guid01,5,pageview
se01,guid01,3,
se01,guid01,2,
se01,guid01,9,
insert  into  table doe34.dws_flow_acc_page_sess_agg partition(dt='2022-10-26')
select
      new_session as session_id ,
      guid ,
      min(ts) as start_ts ,
      max(ts) as end_ts ,
      split(min(if(event_id = 'pageView' , ts||'-'||properties['url'] , null)) ,'-')[1] as  enter_page ,
      split(max(if(event_id = 'pageView' , ts||'-'||properties['url'] , null)) ,'-')[1] as  exit_page ,
      if(sum(if(event_id = 'pageView', 1, 0))>1 , 0 , 1) as is_jump_out ,
      sum(if(event_id = 'pageView', 1, 0)) as  session_pv
from doe34.dwd_app_event_log_dtl where dt = '2022-10-26'
group by  new_session, guid
;
/*

1,pageView,"{""pageId"":""c056"",""refUrl"":""/search/sea0272.html"",""url"":""/courses/griffin/c056.html""}"
1,pageView,"{""pageId"":""stu0542"",""refUrl"":""/jobs/job0963.html"",""url"":""/students/stu0542.html""}"
4,pageView,"{""pageId"":""con0030"",""refUrl"":""/contacts/con0304.html"",""url"":""/contacts/con0030.html""}"

 */
select
guid ,
       event_id ,
       properties ,
       ts
from doe34.dwd_app_event_log_dtl where dt = '2022-10-26' and event_id = 'pageView' ;
--------------------------------------------用户粒度表------------------------------
drop  table if exists  doe34.dws_flow_acc_page_user_agg ;
create table if not exists   doe34.dws_flow_acc_page_user_agg(
    guid bigint ,
    session_cnt int ,
    acc_long  bigint ,
    jump_out_cnt int ,
    pv int ,
    is_new  int ,
    province string ,
    city  string  ,
    area string
)
partitioned by (dt string)
stored as orc
tblproperties ('orc.compress'='snappy') ;


set hive.exec.mode.local.auto=true;
insert  into  table  doe34.dws_flow_acc_page_user_agg partition (dt='2022-10-26')
select
    o1.guid ,
    o1.session_cnt,
    o1.acc_long,
    o1.jump_out_cnt,
    o1.pv ,
    if(nvl(o2.uid ,o3.tmp_guid) is not null , 1 , 0 )  as  is_new ,
    o1.province,
    o1.city,
    o1.area
       from
(
            select
                   t1.guid ,
                   t1.session_cnt,
                   t1.acc_long,
                   t1.jump_out_cnt,
                   t1.pv ,
                   t2.province,
                   t2.city,
                   t2.area
            from
                 (select
                      guid ,
                      count(session_id) as session_cnt ,
                      sum(end_ts - start_ts) acc_long ,
                      sum(is_jump_out) as jump_out_cnt ,
                      sum(session_pv) as pv
                  from  doe34.dws_flow_acc_page_sess_agg where dt = '2022-10-26'
                group by   guid
                     ) t1
            join
             (
                 select
                 guid ,
                        max(province) province ,
                        max(city) city ,
                        max(area) area
                   from doe34.dwd_app_event_log_dtl where dt = '2022-10-26'
                 group by guid
                 ) t2
            on  t1.guid = t2.guid
)o1
left join
    (
       select
       uid
        from doe34.service_account_info  where  register_date = '2022-10-26'
    )o2
on  o1.guid = o2.uid
left join
    (
        select
        tmp_guid
        from  doe34.app_device_tmp_guid2 where dt = '2022-10-26'
        )o3
on  o1.guid = o3.tmp_guid
;



-- 数仓中的表 要具有反应历史数据变化的功能 , 指定字段 记录数据时间 或者 创建分区表
create  table  doe34.app_device_tmp_guid2(
    device_id string ,
    tmp_guid  bigint
)partitioned by (dt  string)
stored as  orc
tblproperties ('orc.compress' = 'snappy') ;
insert  into  table  doe34.app_device_tmp_guid2  partition (dt='2022-10-26')
select  * from   doe34.app_device_tmp_guid ;

--------------------------------------------页面粒度表------------------------------
drop  table  if exists    doe34.dws_flow_acc_page_dtl  ;
create  table  if  not exists   doe34.dws_flow_acc_page_dtl(
    page_url  string  comment '受访页url' ,
    guid  bigint comment   '用户' ,
    session_id  string comment  '页面所属的会话' ,
    ref_url string  comment   '页面的来源地址'  ,
    ref_type string  comment    '页面来源类型' ,
    acc_long  bigint  comment   '页面访问时长' ,
    ts  bigint   comment   '页面打开时间'
)
partitioned by (dt  string)
stored as  orc
tblproperties ('orc.compress'='snappy') ;
-- 计算 来源  DWD 明细表
select
t1.page_url,
       t1.guid,
       t1.session_id,
       t1.ref_url,
       t1.ref_type,
       lead(t1.ts , 1 , t2.end_ts) over(partition by  t1.session_id  order by  t1.ts)  - t1.ts  as  acc_long ,
       ts
       from
(select
      properties['url']  as  page_url ,
       guid   ,
       new_session as session_id ,
       properties['refUrl']  as  ref_url ,
       case
            when     properties['refUrl'] like  '/%'  then   '站内搜索'
            when     properties['refUrl'] like  'http/%'  then '外部来源'
            else  '其他'
          end   as  ref_type ,
       ts

from doe34.dwd_app_event_log_dtl where  dt = '2022-10-26'  and  event_id ='pageView' )t1
join
    (
        select session_id , end_ts from   doe34.dws_flow_acc_page_sess_agg  where dt = '2022-10-26'

    )t2
on  t1.session_id = t2.session_id ;


-- 计算 来源  DWD 明细表
insert   into  table   doe34.dws_flow_acc_page_dtl  partition(dt='2022-10-26')
select
*
/*    t1.page_url,
    t1.guid,
    t1.session_id,
    t1.ref_type,    t1.ref_url,

   --  (lead(t1.ts , 1 , t2.end_ts) over(partition by  t1.session_id  order by  t1.ts)  -  t1.ts)  as  acc_long ,
    ts*/
from
    (select
         properties['url']  as  page_url ,
         guid   ,
         new_session as session_id ,
         properties['refUrl']  as  ref_url ,
         case
             when     properties['refUrl'] like  '/%'  then   '站内搜索'
             when     properties['refUrl'] like  'http/%'  then '外部来源'
             else  '其他'
             end   as  ref_type ,
         ts

     from doe34.dwd_app_event_log_dtl where  dt = '2022-10-26'  and  event_id ='pageView'
    )t1
        join
       doe34.dws_flow_acc_page_sess_agg  t2
    on  t1.session_id = t2.session_id  and  t2.dt='2022-10-26';


-------------------------------------------------------------------------------------------

-- 需求1  单维度  流量统计    统计每个页面的  pv  uv  flow_pv  exit_cnt  enter_cnt , avg_acc_long
-- 中心事实表  页面明细表
-- 计算来源  页面明细  +  会话粒度聚合表
-- 1 根据需求创建报表
create  table  doe34.ads_flow_acc_page_report_d(
    page_url  string ,
    pv  int ,
    uv  int  ,
    folow_pv  int ,
    exit_cnt int  ,
    enter_cnt int ,
    avg_acc_long  bigint

) partitioned by (dt string)
stored as  orc
tblproperties ('orc.compress'='snappy') ;

A , /B , zss , session01
B , /A , zss , session01
A , /B , zss , session01
A , /C , zss , session01
A , /B , lss , session02
B , /A , lss , session02
A , /B , lss , session02
A , /C , lss , session02
O , /C , lss , session02

A , /B , zss , session03
B , /A , zss , session03
A , /B , zss , session03
A , /C , zss , session03

session01    A
session02    O
session03    A


WITH x1 AS (
    SELECT
        page_url,
        COUNT(1) AS pv,
        COUNT(DISTINCT guid) AS uv,
        AVG(acc_long) AS avg_acc_long
    FROM
        doe34.dws_flow_acc_page_dtl
    WHERE
            dt = '2022-10-26'
    GROUP BY
        page_url
),
     x2 AS (
         --  页面作为引用页面的次数
         SELECT
             ref_url,
             COUNT(1) AS folow_pv
         FROM
             doe34.dws_flow_acc_page_dtl
         WHERE
                 dt = '2022-10-26'  AND ref_url is not null
         GROUP BY
             ref_url
     ),
     x3 AS (
         -- 页面作为退出页的次数
         SELECT
             exit_page,
             COUNT(1) AS exit_cnt
         FROM
             doe34.dws_flow_acc_page_sess_agg
         WHERE
                 dt = '2022-10-26'
         GROUP BY
             exit_page
     ),
     x4 AS (
         -- 页面作为入口页的次数
         SELECT
             enter_page,
             COUNT(1) AS enter_cnt
         FROM
             doe34.dws_flow_acc_page_sess_agg
         WHERE
                 dt = '2022-10-26'
         GROUP BY
             enter_page
     )
INSERT INTO
    TABLE doe34.ads_flow_acc_page_report_d PARTITION(dt = '2022-10-26')
SELECT
    x1.page_url AS page_url,
    x1.pv AS pv,
    x1.uv AS uv,
    NVL(x2.folow_pv, 0) AS folow_pv ,
    NVL(x3.exit_cnt, 0) AS exit_cnt,
    NVL(x4.enter_cnt, 0) AS enter_cnt,
    x1.avg_acc_long AS avg_acc_long
FROM
    x1
        LEFT JOIN x2 ON x1.page_url = x2.ref_url
        LEFT JOIN x3 ON x1.page_url = x3.exit_page
        LEFT JOIN x4 ON x1.page_url = x4.enter_page;


需求2  :
新老属性下 页面的  pv  uv  folow_pv  exit_cnt  enter_cnt   avg_acc_long
-- 数据来源
-- 中心事实表  页面明细
-- 会话聚合表   用户明细表
/*select
    t1.page_url  ,
    t2.is_new
from
    (
        select
            *
        from
            doe34.dws_flow_acc_page_dtl where dt = '2022-10-26'
    )t1
        join
    doe34.dws_flow_acc_user_agg  t2
    on t1.guid = t2.guid*/
