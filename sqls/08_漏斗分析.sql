-- 漏斗分析
guid,event_id,properties,ts
1,e3,p1:v2,10
1,e1,p1:v1_p2:v2,11
1,e2,p2:v2,12
1,e3,p2:v2,13
1,e4,p2:v2,14
1,e2,p2:v2,15
1,e1,p1:v1,16
1,e3,p1:v2_p5:v5,17
1,e4,p2:v2,18
1,e5,p5:v5,20
1,e1,p1:v1,19
2,e3,p1:v1_p2:v2,11
2,e2,p2:v2,12
2,e3,p2:v2,13
2,e5,p5:v2,14
2,e2,p2:v2,15
2,e1,p1:v1,16
2,e4,p1:v1_p5:v5,17
2,e3,p1:v2,18
2,e4,p2:v2,19
3,e1,p1:v1_p2:v2,11
3,e2,p2:v2,12
3,e1,p2:v2,13
3,e5,p5:v2,14
3,e4,p2:v2,15
3,e3,p1:v2,16
3,e1,p1:v1_p5:v5,17
3,e4,p1:v2,18

create  table  if not exists  doe34.test_event_funnel(
                                                         guid  bigint ,
                                                         event_id  string ,
                                                         properties map<string , string> ,
                                                         ts bigint
)
    row format delimited  fields terminated by  ','
        collection items terminated by '_'
        map keys terminated by ':'
;
load  data  local  inpath   '/data/funnal.csv' into table  doe34.test_event_funnel ;
-- e1_p1:v1  e2  e3  e4_p2:v2   e5    [没有时间间隔要求  没有事件强关联]
select
    guid ,
    case

        when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4.*e5'  then  5
        when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4'  then  4
        when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3'  then  3
        when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2'  then  2
        when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1'  then  1
        else  0 end  as  step
from doe34.test_event_funnel
where (event_id = 'e1' and properties['p1'] = 'v1')
   or event_id = 'e2'
   or event_id = 'e3'
   or (event_id='e4' and properties['p2'] = 'v2')
   or event_id = 'e5'
group by  guid ;

+-------+-------+
| guid  | step  |
+-------+-------+
| 1     | 5     |1  1  1   1  1
| 2     | 1     |1  0  0   0  0
| 3     | 3     |1  1  1   0  0
+-------+-------+
with  x  as (
    select
        guid ,
        case

            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4.*e5'  then  5
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4'  then  4
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3'  then  3
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2'  then  2
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1'  then  1
            else  0 end  as  step
    from doe34.test_event_funnel
    where (event_id = 'e1' and properties['p1'] = 'v1')
       or event_id = 'e2'
       or event_id = 'e3'
       or (event_id='e4' and properties['p2'] = 'v2')
       or event_id = 'e5'
    group by  guid
)
select
    sum(if(step >=1 , 1 , 0)) as step_1 ,
    sum(if(step >=2 , 1 , 0))  as step_2 ,
    sum(if(step >=3 , 1 , 0))  as step_3 ,
    sum(if(step >=4 , 1 , 0))  as step_4 ,
    sum(if(step >=5 , 1 , 0))  as step_5
from x ;
--  e1   e2  e3 e4 e5

+---------+---------+---------+---------+---------+
| step_1  | step_2  | step_3  | step_4  | step_5  |
+---------+---------+---------+---------+---------+
| 3       | 2       | 2       | 1       | 1       |
+---------+---------+---------+---------+---------+




总人数 :   日活用户

漏斗的入口为总人数

秒杀抢购的活动


参与秒杀活动   10000
秒杀      1000
抢购成功   100
支付       99
支付成功    98




+---------+---------+---------+---------+---------+
| step_1  | step_2  | step_3  | step_4  | step_5  |
+---------+---------+---------+---------+---------+
| 3       | 2       | 2       | 1       | 1       |
+---------+---------+---------+---------+---------+

create  table  if  not exists   doe34.ads_app_funnal_rept(
                                                             compute_dt  string ,
                                                             funnel_name  string ,
                                                             total_cnt  int ,  -- 漏斗入口的总人数  完成第一个入口事件的总人数
                                                             step_cnt map<string , int>
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy') ;


;
with  x  as (
    select
        guid ,
        case

            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4.*e5'  then  5
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4'  then  4
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3'  then  3
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2'  then  2
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1'  then  1
            else  0 end  as  step
    from doe34.test_event_funnel
    where (event_id = 'e1' and properties['p1'] = 'v1')
       or event_id = 'e2'
       or event_id = 'e3'
       or (event_id='e4' and properties['p2'] = 'v2')
       or event_id = 'e5'
    group by  guid
)
select
    '2022-10-26'  as  compute_dt ,
    '秒杀转化漏斗' as  funnel_name ,
    sum(if(step >= 1, 1, 0)) as total ,
    map('step1', sum(if(step >= 1, 1, 0)) ,
        'step2', sum(if(step >= 2, 1, 0)) ,
        'step3', sum(if(step >= 3, 1, 0)) ,
        'step4', sum(if(step >= 4, 1, 0)) ,
        'step5', sum(if(step >= 5, 1, 0))
        )  as  step_cnt
from x ;

+-------------+--------------+--------+----------------------------------------------------+
| compute_dt  | funnel_name  | total  |                      step_cnt                      |
+-------------+--------------+--------+----------------------------------------------------+
| 2022-10-26  | 秒杀转化漏斗       | 3      | {"step1":3,"step2":2,"step3":2,"step4":1,"step5":1} |
+-------------+--------------+--------+----------------------------------------------------+


with  x  as (
    select
        guid ,
        case

            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4.*e5'  then  5
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3.*e4'  then  4
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2.*e3'  then  3
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1.*e2'  then  2
            when concat_ws(',' ,sort_array(collect_list(ts||'-'||event_id)))    regexp  'e1'  then  1
            else  0 end  as  step
    from doe34.test_event_funnel
    where (event_id = 'e1' and properties['p1'] = 'v1')
       or event_id = 'e2'
       or event_id = 'e3'
       or (event_id='e4' and properties['p2'] = 'v2')
       or event_id = 'e5'
    group by  guid
)
select
    compute_dt,
    funnel_name,
    total,
    k,
    v ,
    cast((v/total)*100 as decimal(5,2))||'%'
from (
         select '2022-10-26'               as compute_dt,
                '秒杀转化漏斗'                 as funnel_name,
                sum(if(step >= 1, 1, 0)) as total,
                map('step1', sum(if(step >= 1, 1, 0)),
                    'step2', sum(if(step >= 2, 1, 0)),
                    'step3', sum(if(step >= 3, 1, 0)),
                    'step4', sum(if(step >= 4, 1, 0)),
                    'step5', sum(if(step >= 5, 1, 0))
                    )                    as step_cnt
         from x
     ) t
         lateral view   explode(step_cnt)  tmp as  k,v
;


+-------------+--------------+--------+--------+----+
| compute_dt  | funnel_name  | total  |   k    | v  |
+-------------+--------------+--------+--------+----+
| 2022-10-26  | 秒杀转化漏斗       | 3      | step1  | 3  |
| 2022-10-26  | 秒杀转化漏斗       | 3      | step2  | 2  |
| 2022-10-26  | 秒杀转化漏斗       | 3      | step3  | 2  |
| 2022-10-26  | 秒杀转化漏斗       | 3      | step4  | 1  |
| 2022-10-26  | 秒杀转化漏斗       | 3      | step5  | 1  |
+-------------+--------------+--------+--------+----+