-- 维护用户活跃拉链表
-- 根据  T-1日的拉链表 和 T日的用户日活表  合并
-- 初始化拉链表  guid , first_login , first_login , 9999-12-31
-- 可以使用分区字段  保存历史上的拉链表数据
create table if not exists doe34.test_dws_app_uac_range
(
    guid        bigint,
    first_login string,
    start_dt    string,
    end_dt      string
)
    row format delimited fields terminated by ',';

load data local inpath '/data/lalian/range.csv' into table doe34.test_dws_app_uac_range;
2022-10-25的拉链表

1,2022-10-01,2022-10-01,2022-10-09
1,2022-10-01,2022-10-11,2022-10-20
1,2022-10-01,2022-10-23,9999-12-31
2,2022-10-01,2022-10-01,2022-10-17
2,2022-10-01,2022-10-21,9999-12-31
3,2022-10-01,2022-10-01,9999-12-31
4,2022-10-01,2022-10-01,2022-10-22

create table doe34.test_dws_app_uac_dtl
(
    guid        bigint,
    first_login string
)
    partitioned by (dt string)
    row format delimited fields terminated by ',';
load data local inpath '/data/lalian/uac.csv' into table doe34.test_dws_app_uac_dtl partition (dt = '2022-10-26');

2022-10-26 用户活跃数据
guid ,first_login
1,2022-10-01
3,2022-10-01
4,2022-10-01
5,2022-10-26
---->


/*
-- 分析步骤
2022-10-26 拉链表

第一部分 成为历史状态的数据 不会变化
1,2022-10-01,2022-10-01,2022-10-09
1,2022-10-01,2022-10-11,2022-10-20
2,2022-10-01,2022-10-01,2022-10-17
4,2022-10-01,2022-10-01,2022-10-22
第二部分   没有闭合的数据     闭合(今天没来)  不变(今天出现的用户)
1,2022-10-01,2022-10-23,9999-12-31
2,2022-10-01,2022-10-21,2022-10-25
3,2022-10-01,2022-10-01,9999-12-31
第三部分  完全称为历史状态的数据
4,2022-10-01,2022-10-26,9999-12-31
5,2022-10-26,2022-10-26,9999-12-31


1,2022-10-01,2022-10-23,9999-12-31
2,2022-10-01,2022-10-21,9999-12-31
3,2022-10-01,2022-10-01,9999-12-31
    full  join
1,2022-10-01
3,2022-10-01
4,2022-10-01
5,2022-10-26
 */

select *
from doe34.test_dws_app_uac_dtl
where dt = '2022-10-26';
select *
from doe34.test_dws_app_uac_range;


-- 更新最新的拉链数据   本需求中只有最新的拉链数据   使用的方式是覆盖
insert  overwrite    table  doe34.test_dws_app_uac_range
select guid,
       first_login,
       start_dt,
       end_dt
from doe34.test_dws_app_uac_range
where end_dt != '9999-12-31'
union
select nvl(t1.guid, t2.guid)                           as guid,
       nvl(t1.first_login, t2.first_login)             as first_login,
       nvl(t1.start_dt, t2.dt)                         as start_dt,
       if(t2.guid is null, '2022-10-25', '9999-12-31') as end_dt
from (select guid, first_login, start_dt, end_dt
      from doe34.test_dws_app_uac_range
      where end_dt = '9999-12-31'
     ) t1
         full join
     (
         select guid,
                first_login,
                dt
         from doe34.test_dws_app_uac_dtl
         where dt = '2022-10-26'
     ) t2
     on t1.guid = t2.guid;


-- 统计每个人的总活跃天数
select datediff('2022-10-09' , '2022-10-01') + 1;

select
    guid ,
    sum(datediff(IF(end_dt = '9999-12-31' , '2022-10-26' , end_dt) , start_dt) + 1) as  total
from doe34.test_dws_app_uac_range
group by guid  ;


-- 最近14天  每个人的总活跃天数
--  过滤出范围内的数据   end_dt  >= date_sub('2022-10-26' , 13)
--   计算时的起始时间  小于指定范围   修改起始时间为起点
select  date_sub('2022-10-26' , 13) ;

select
    guid ,
    sum(datediff(IF(end_dt = '9999-12-31' , '2022-10-26' , end_dt) , if(start_dt <  date_sub('2022-10-26' , 13)  , '2022-10-13' , start_dt)  ) + 1) as  total
from doe34.test_dws_app_uac_range
where  end_dt >=  date_sub('2022-10-26' , 13)   -- 2022-10-13
group by guid  ;

-- 最近7天  每个人的总活跃天数
-- 最近30天  每个人的总活跃天数
--  ... ...

-- 统计每个人的最大连续活跃天数


select
    guid ,
    max(datediff(IF(end_dt = '9999-12-31' , '2022-10-26' , end_dt) ,  start_dt  ) + 1) as  max_act_days
from doe34.test_dws_app_uac_range
group by guid  ;

最近14天用户的最大活跃天数

select
    guid ,
    max(datediff(IF(end_dt = '9999-12-31' , '2022-10-26' , end_dt) ,  if( start_dt < date_sub('2022-10-26' , 13)  ,  date_sub('2022-10-26' , 13)  ,start_dt    )  ) + 1) as  max_act_days
from doe34.test_dws_app_uac_range
where  end_dt >=  date_sub('2022-10-26' , 13)
group by guid  ;

/*
最近7天用户的最大活跃天数
最近30天用户的最大活跃天数*/


;


-- 每个人的最大沉默天数
select
    guid ,
    max(silence_days)   max_silence_days
from
    (select
         guid ,
         start_dt ,
         end_dt ,
         datediff(start_dt ,lag(end_dt , 1 ,date_add(start_dt,-1))  over (partition  by guid  order by start_dt))  - 1  as  silence_days
     from doe34.test_dws_app_uac_range
    ) t
group by  guid;

-- 每个人的总沉默天数
select
    guid ,
    sum(silence_days)   max_silence_days
from
    (select
         guid ,
         start_dt ,
         end_dt ,
         datediff(start_dt ,lag(end_dt , 1 ,date_add(start_dt,-1))  over (partition  by guid  order by start_dt))  - 1  as  silence_days
     from doe34.test_dws_app_uac_range
    ) t
group by  guid;


-- 最近15天的最大沉默天数

select
    '2022-10-20'   as  comput_dt ,
    count(1)
from doe34.test_dws_app_uac_range
where  '2022-10-20' between  start_dt  and  end_dt ;

with  x  as (
    select '2022-10-24' as comput_dt,
           count(1)     as cnt
    from doe34.test_dws_app_uac_range
    where '2022-10-24' between start_dt and end_dt
    union
    select '2022-10-25' as comput_dt,
           count(1)     as cnt
    from doe34.test_dws_app_uac_range
    where '2022-10-25' between start_dt and end_dt
    union
    select '2022-10-26' as comput_dt,
           count(1)     as cnt
    from doe34.test_dws_app_uac_range
    where '2022-10-26' between start_dt and end_dt
)
select
    collect_list(map(comput_dt, cnt))[0]['2022-10-24']   as  `2022-10-24`  ,
    collect_list(map(comput_dt, cnt))[1]['2022-10-25']   as  `2022-10-25`  ,
    collect_list(map(comput_dt, cnt))[2]['2022-10-26']   as  `2022-10-26`
from x ;

+-------------+-------------+-------------+
| 2022-10-24  | 2022-10-25  | 2022-10-26  |
+-------------+-------------+-------------+
| 3           | 3           | 4           |
+-------------+-------------+-------------+

+----------------+----------+
| _u2.comput_dt  | _u2.cnt  |
+----------------+----------+
| 2022-10-20     | 3        |
| 2022-10-21     | 3        |
| 2022-10-22     | 3        |
| 2022-10-23     | 4        |
| 2022-10-24     | 3        |
| 2022-10-25     | 4        |
| 2022-10-26     | 4        |
+----------------+----------+

x
 2022-10-20 ,  2022-10-21 ,  2022-10-22 ,  2022-10-23 ,  2022-10-24 ,  2022-10-25 ,  2022-10-26
    3             3             3               4             3            4             4


新老的定义 :
每天统计  新老访客数 2022-10-01  新:123213  老:424232
每天统计  新老访客数 2022-10-02  新:123343  老:124232
每天统计  新老访客数 2022-10-03  新:139213  老:34232
每天统计  新老访客数 2022-10-04  新:124513  老:384232

select
    sum(if(first_login = '2022-10-01' , 1, 0))  as is_new_cnt ,
    sum(if(first_login != '2022-10-01' , 1, 0))  as is_old_cnt
from doe34.test_dws_app_uac_range
where   '2022-10-01'  between   start_dt  and  end_dt ;


-- select  floor_week(current_timestamp()) ;
-- select  date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2)));
周报表:  周一计算 [获取上一周的所有的数据]


select
    guid  ,
    sum(datediff( if(end_dt = '9999-12-31' , '2022-10-26' , end_dt)   , if(start_dt < date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2))) , date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2))) , start_dt)) +1 ) as  days

from
    doe34.test_dws_app_uac_range
where  end_dt >= date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2)))
group by  guid
              +-------+-------+
              | guid  | days  |
          +-------+-------+
              | 1     | 3     |
              | 2     | 2     |
              | 3     | 3     |
              | 4     | 1     |
              | 5     | 1     |
          +-------+-------+

    口径1  以一个人的实际登录天数为统计频次依据

with  tmp as (
    select
        guid  ,
        sum(datediff( if(end_dt = '9999-12-31' , '2022-10-26' , end_dt)   , if(start_dt < date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2))) , date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2))) , start_dt)) +1 ) as  days
    from
        doe34.test_dws_app_uac_range
    where  end_dt >= date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2)))
    group by  guid
)

select
    tmp.days ,
    count(1) uac_cnt  ,
    (count(1) / max(x1.total_uac_cnt) *100) ||'%'
from tmp
         join
     (select
          count(1)  as  total_uac_cnt
      from tmp) x1
group by  tmp.days  ;



--口径2  以一个人的实际登录天数的范围  为统计频次依据
--比如 一个登录3次  会被算在  登录 1次  2次  3次  的里面

with  tmp as (
    select
        guid  ,
        sum(datediff( if(end_dt = '9999-12-31' , '2022-10-26' , end_dt)   , if(start_dt < date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2))) , date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2))) , start_dt)) +1 ) as  days
    from
        doe34.test_dws_app_uac_range
    where  end_dt >= date_sub('2022-10-26' , if(dayofweek('2022-10-26') == '1' , 6 ,(dayofweek('2022-10-26')-2)))
    group by  guid
)  -- 一个人和一周内的总登录天数
select
    sum(if(days >=1 , 1 , 0)) as  one_days ,
    sum(if(days >=2 , 1 , 0 )) as two_days ,
    sum(if(days >=3 , 1 , 0 )) as three_days  ,
    sum(if(days >=4 , 1 , 0 )) as four_days  ,
    sum(if(days >=5 , 1 , 0 )) as five_days  ,
    sum(if(days >=6 , 1 , 0 )) as six_days  ,
    sum(if(days >=7 , 1 , 0 )) as seven_days  ,
    sum(if(days > 2 , 1 , 0 ))/count(1)  as three_days_s  ,
    sum(if(days <= 2 , 1 , 0 ))/count(1)  as three_days_s  ,
    count(1) as  total_cnt
from
    tmp
;

+-------+-------+
| guid  | days  |
+-------+-------+
| 1     | 3     |
| 2     | 2     |
| 3     | 3     |
| 4     | 1     |
| 5     | 1     |
+-------+-------+
