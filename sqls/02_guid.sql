-- 设置本地运行模式
set hive.exec.mode.local.auto=true;


-- 集成地理位置后的用户行为数据
-- doe34.tmp_app_log_split_areas
select *
from doe34.tmp_app_log_split_areas where dt = '2022-10-26';

-- 业务系统用户注册表  (模拟)  从业务库中每天增量导入注册用户信息
drop table if exists doe34.service_account_info;
create table if not exists doe34.service_account_info
(
    uid           bigint,
    account       string,
    register_date string
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy')
;

insert into table doe34.service_account_info
select row_number() over () as uid,
       account,
       '2022-10-26'         as register_date
from doe34.tmp_app_log_split_areas
where dt = '2022-10-26'
  and account is not null
  and trim(account) != ''
group by account
;
-- 设备和账号权重表
-- 根据用户的访问次数 计算权重信息 规则:  +  根据用户的访问次数*10   权重*80% 衰减
drop table if exists doe34.app_device_account_bind;

create table if not exists doe34.app_device_account_bind
(
    device_id string comment '设备ID',
    account   string comment '账号',
    weight    double comment '权重值'
)
    partitioned by (dt string)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');

-- 计算2022-10-26日的设备和账号权重
-- 计算的数据来源  2022-10-26用户行为数据   2022-10-25的设备和账号权重表
/*
  account01,device01,A,session01
  account01,device01,B,session01
  account01,device01,C,session01
  account01,device01,D,session01
  account01,device01,A,session02
  account01,device01,X,session02
  account01,device01,O,session02

  account02,device02,A,session03
  account02,device02,B,session03
  account02,device02,C,session03
  account02,device02,D,session04
  account02,device02,A,session04
  account02,device02,X,session04
  account02,device02,O,session05

  account03,device03,A,session03
  account03,device03,B,session03
 */

/*
  account01,device01,88
  account02,device02,99
  account04,device04,80
 */

/*
  account01,device01,2
  account02,device02,3
  account03,device03,1
 */
/*
account01,device01,2     ,    account01,device01,88
account02,device02,3     ,    account02,device02,99
null , null , null       ,    account04,device04,80
account03,device03,1     ,    null , null ,null
 */


------------------------------------------------------------------------------------------
with t1 as (
    select account,
           device_id,
           count(distinct new_session) as seesion_cnt
    from doe34.tmp_app_log_split_areas
    where dt = '2022-10-26'
      and account is not null
      and trim(account) != ''
    group by account, device_id
),
     t2 as (
         select account,
                device_id,
                weight
         from doe34.app_device_account_bind
         where dt = '2022-10-25'
     )
-- t1  t2 重复使用   主逻辑清晰
insert
into doe34.app_device_account_bind partition (dt = '2022-10-26')
select nvl(t1.account, t2.account)                                as account,
       nvl(t1.device_id, t2.device_id)                            as device_id,
       case
           when t1.device_id is not null and t2.device_id is not null then t1.seesion_cnt * 10 + t2.weight
           when t1.device_id is null then t2.weight * 0.8
           when t2.device_id is null then t1.seesion_cnt * 10 end as weight
from t1
         full join
     t2
     on t1.account = t2.account and t1.device_id = t2.device_id
;
-------------------------------------------------------------------------------------------------------------

select account,
       device_id,
       new_session,
       event_id
from doe34.tmp_app_log_split_areas
where dt = '2022-10-26'
  and account = 'aed,uqld' ;


select account,
       device_id,
       count(distinct new_session) as seesion_cnt
from doe34.tmp_app_log_split_areas
where dt = '2022-10-26'
  and account = 'aed,uqld'
group by account, device_id;


-- 空设备临时guid表   空设备:从来没有出现过账号的设备
drop table doe34.app_device_tmp_guid;
create table if not exists doe34.app_device_tmp_guid
(
    device_id string, -- A
    tmp_guid  bigint  -- 1011111101
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy');
--维护 2022-10-26日的设备临时guid表的数据
-- 用户行为明细表 中 有account  今天没有账号的数据且此设别没有权重账号
-- 1)  过滤出今天完全没有账号的设备 , 今天所有的用户行为数据中都有不存在账号信息
-- 2)  如果在2022-10-25日的设备和账号权重表中, 有账号数据
-- 3)  如果在2022-10-25日的设别临时guid表中  ,  没有此设备
-- 4)       生成临时的guid

/*,A,device01
zss,A,device01

,A,device02
,B,device02  √

,A,device03
,B,device03    device03   tmp_10000000101*/

-- 第一步:  过滤出今天的用户行为数据中  完全没有账号的设备

/*
  A,device01
  ,device01
  ,device01
  ,device02
  B,device02
  A,device02
  ,device03
  ,device03
  ,device03
  ,device04
  ,device04
  ,device04

  create  table doe34.test_all_empty(
    account string ,
    device_id string
  )row format delimited  fields terminated by  ',' ;

   load  data local inpath '/data/test_empty.txt' into  table  doe34.test_all_empty ;
    select
    device_id
    from
        doe34.test_all_empty
    group by  device_id
    having  max(if(trim(account) = '' , null , account))  is null  ;

 */

-- 第二步 : 关联 设备和账号绑定表  留下  没有关联上
-- 第三部 : 关联临时guid表  留下没有关联上的   生成临时guid
----------------------------------------------------------------------------------
insert into  table  doe34.app_device_tmp_guid
select
       x.device_id ,
        row_number() over () + y.max_id  as tmp_guid
       from
        (select
        t1.device_id  as device_id
        from
            (select
               device_id
            from doe34.tmp_app_log_split_areas
             where dt = '2022-10-26'
            group by device_id
            having max(if(trim(account) = '', null, account)) is null
            )t1
        --  第二步: 设备和账号权重表中的数据    没有出现过的设备
        left join
            (select
            device_id
            from
            doe34.app_device_account_bind where  dt = '2022-10-26'
            )t2
             on  t1.device_id = t2.device_id
        left join
            (
             select
             device_id
             from doe34.app_device_tmp_guid
             ) t3
              on  t1.device_id = t3.device_id
            where t2.device_id is null  and t3.device_id is null
) x
  join
(
    select
        nvl(max(tmp_guid) , 1000000000) max_id
    from
        doe34.app_device_tmp_guid
)y
 ;
----------------------------------------------------------------------------------------------
--  2022-10-26天的用户行为数据
--     用户注册信息
--  2022-10-26  设备账号权重表
--     临时guid表

-- 为每条数据 打上唯一的guid标识
-- ************************************
-- 如果这条数据  有账号  关联用户注册表 中的uid

alter table   doe34.dwd_app_event_log_dtl drop partition(dt='2022-10-26') ;
--------------------------------------------------------------------------------------------------
insert   into  table  doe34.dwd_app_event_log_dtl   partition(dt='2022-10-26')
select * from
(select
t2.uid as  guid ,
t1.account, app_id, app_version, carrier, device_id, device_type, event_id, ip, latitude, longitude, net_type, os_name, os_version, properties, release_channel, resolution, session_id, new_session, province, city, area, ts
from
( select
    *
    from
         doe34.tmp_app_log_split_areas where dt = '2022-10-26'
    and  account is not null  and  trim(account) != ''
)t1
join
doe34.service_account_info t2
on  t1.account = t2.account
union
-- ************************************
-- 如果这条数据  没账号的明细数据
     -- 1 关联设备和账号权重表   权重最大的账号  对应的用户的id
select
nvl(tb_device_uid.uid , tb_tmp_guid.tmp_guid) as guid ,
event_data.account, app_id, app_version, carrier, event_data.device_id, device_type, event_id, ip, latitude, longitude, net_type, os_name, os_version, properties, release_channel, resolution, session_id, new_session, province, city, area, ts
from
     (select
          *
      from
          doe34.tmp_app_log_split_areas where dt = '2022-10-26' and (account is null or trim(account) = '')
         ) event_data
       left join
           -- 设备 最大权重账号
              (select
                x2.device_id ,x3.uid
                from   (select
                            device_id ,account
                        from
                            (select * ,
                                    row_number() over (partition by device_id order by weight desc) as rn
                             from doe34.app_device_account_bind where  dt = '2022-10-26'
                            )x1 where rn = 1) x2
                join   doe34.service_account_info  x3
                on x2.account = x3.account
            ) tb_device_uid
           on event_data.device_id = tb_device_uid.device_id
      left join  doe34.app_device_tmp_guid as  tb_tmp_guid
          on event_data.device_id = tb_tmp_guid.device_id) p
where  guid is not null
;
-------------------------------------------------------------------------------------------------
--  guid生成的流程
--  session分割 , 地理位置集成  , 统一数据格式 , 统一字段命名 , 创建全局的guid
--  将上面的结果数据  保存在DWD 数仓明细层中

drop  table  if   exists doe34.dwd_app_event_log_dtl ;
    create  table if not exists  doe34.dwd_app_event_log_dtl
    (
    guid bigint ,
    account string ,
    app_id string ,
    app_version string ,
    carrier string ,
    device_id string ,
    device_type string ,
    event_id string ,
    ip string ,
    latitude double ,
    longitude double ,
    net_type string ,
    os_name string ,
    os_version string ,
    properties map<string,string>  ,
    release_channel string ,
    resolution string ,
    session_id string ,
    new_session string ,
    province string ,
    city string ,
    area string ,
    ts bigint
)
partitioned by (dt string)
stored as  orc
tblproperties ('orc.compress'='snappy') ;


