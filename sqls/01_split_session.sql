--测试session分割的sql代码
-- 数据  uid , event_id , session_id , ts
-- 事件大于30秒切割
1,A,DGSJDGFFS,1
1,B,DGSJDGFFS,2
1,C,DGSJDGFFS,3
1,D,DGSJDGFFS,40
1,E,DGSJDGFFS,50
1,F,GHDSIUJKH,89
1,D,GHDSIUJKH,90
1,A,GHDSIUJKH,131
1,G,GHDSIUJKH,179
2,A,DGSJJKFFS,1
2,B,DGSJJKFFS,2
2,C,DGSJJKFFS,3
2,D,DGSJJKFFS,40
2,E,DGSJJKFFS,50
2,F,GHPOIUJKH,89
2,D,GHPOIUJKH,90
2,A,GHPOIUJKH,131
2,G,GHPOIUJKH,179

create table doe34.test_split_session(
   uid int ,
   event_id string ,
   session_id  string ,
   ts bigint
) row format delimited  fields terminated by  "," ;
-- 导入数据
load data  local  inpath  '/data/session/' overwrite  into table  doe34.test_split_session ;


--session分割逻辑
select
    uid ,
    session_id ,
    ts ,
    concat(session_id , '-' ,  sum(flag) over(partition by  uid , session_id  order by ts)) as new_session
from
    (
        select
            uid ,
            session_id ,
            ts,
            if((ts - lag(ts ,1 , ts)  over(partition by uid , session_id order by ts))>30 , 1, 0 ) as flag
        from
            doe34.test_split_session
) t ;