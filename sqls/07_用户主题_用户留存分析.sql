
show  tables  from  doe34 ;
desc   doe34.dws_flow_acc_page_user_agg ;

desc doe34.test_dws_app_uac_range ;
-- 用户活跃区间表
guid , first_login , start_dt , end_dt

-- 计算计算日用户的总人数   , 计算日的用户信息
comput_dt  between  start_dt  and  end_dt   -- 计算日活跃的用户信息
   where  first_login   在7天以内的用户  -- 过滤出
-- 用户活跃区间表  ------|| 中间过度表 || --------> 报表

元数据管理  维护表的血源关系

A join  B   --> C

create  table  doe34.test_dws_app_uac_retention(
                                                   comput_dt  string ,
                                                   first_login  string ,
                                                   uac_cnt  string  , -- 留存范围内的活跃人数
                                                   ret_days int ,
                                                   ret_cnt int
)
    stored as orc
    tblproperties ('orc.compress' = 'snappy')
;

-- 计算日的活跃用户
insert  into  table doe34.test_dws_app_uac_retention
select
    '2022-10-26' as comput_dt ,
    first_login ,
    max(uac_cnt) as  uac_cnt ,
    max(ret_days) as  ret_days ,
    max(ret_cnt) as  ret_cnt
from
    (select
         '2022-10-26' as comput_dt ,
         first_login ,
         count(1) over()  as uac_cnt ,
         datediff('2022-10-26'  , first_login)  as ret_days ,
         count(1)  over(partition by first_login) as ret_cnt
     from doe34.test_dws_app_uac_range
     where end_dt = '9999-12-31'
       and  first_login >= date_sub('2022-10-26' , 7)) t
where  ret_days != 0
group by first_login  ;

-- 最近7天注册的人  ; 今天活跃的最近7天注册人的总人数

+-------------+--------------+----------+-----------+----------+
|  comput_dt  | first_login  | uac_cnt  | ret_days  | ret_cnt  |
+-------------+--------------+----------+-----------+----------+
| 2022-10-26  | 2022-10-20   | 26       | 6         | 4        |
| 2022-10-26  | 2022-10-21   | 26       | 5         | 3        |
| 2022-10-26  | 2022-10-22   | 26       | 4         | 5        |
| 2022-10-26  | 2022-10-23   | 26       | 3         | 1        |
| 2022-10-26  | 2022-10-24   | 26       | 2         | 4        |
| 2022-10-26  | 2022-10-25   | 26       | 1         | 5        |
+-------------+--------------+----------+-----------+----------+
set hive.exec.mode.local.auto=true;

select
    *
from
    doe34.test_dws_app_uac_retention where  comput_dt = '2022-10-26' ;

--报表
日期         人数     1  2  3  4  5  6  7
2022-10-26    26     5  4  1  5  3  4  x

select
    comput_dt , uac_cnt ,
    max(if(ret_days == 1 , ret_cnt , null))  as `次日` ,
    max(if(ret_days == 2 , ret_cnt , null))  as `第2日` ,
    max(if(ret_days == 3 , ret_cnt , null))  as `第3日` ,
    max(if(ret_days == 4 , ret_cnt , null))  as `第4日` ,
    max(if(ret_days == 5 , ret_cnt , null))  as `第5日` ,
    max(if(ret_days == 6 , ret_cnt , null))  as `第6日`  ,
    max(if(ret_days == 7 , ret_cnt , null))  as `第7日`
from
    doe34.test_dws_app_uac_retention where  comput_dt = '2022-10-26'
group by  comput_dt , uac_cnt
;

-- 计算日    2022-10-26

首登日        总人数      次日    第2日   第3日   第4日
2022-10-23    111         99    98      96    null





select
    *
from
    doe34.test_dws_app_uac_retention where  comput_dt = '2022-10-26' ;

+-------------+--------------+----------+-----------+----------+
|  comput_dt  | first_login  | uac_cnt  | ret_days  | ret_cnt  |
+-------------+--------------+----------+-----------+----------+
| 2022-10-26  | 2022-10-19   | 26       | 7         | 4        |
| 2022-10-26  | 2022-10-20   | 26       | 6         | 4        |
| 2022-10-26  | 2022-10-21   | 26       | 5         | 3        |
| 2022-10-26  | 2022-10-22   | 26       | 4         | 5        |
| 2022-10-26  | 2022-10-23   | 26       | 3         | 1        |
| 2022-10-26  | 2022-10-24   | 26       | 2         | 4        |
| 2022-10-26  | 2022-10-25   | 26       | 1         | 5        |
+-------------+--------------+----------+-----------+----------+
| 2022-10-25  | 2022-10-19   | 28       | 6         | 4        |
| 2022-10-25  | 2022-10-20   | 28       | 5         | 4        |
| 2022-10-25  | 2022-10-21   | 28       | 4         | 3        |   2022-10-25 计算
| 2022-10-25  | 2022-10-22   | 28       | 3         | 5        |
| 2022-10-25  | 2022-10-23   | 28       | 2         | 1        |
| 2022-10-25  | 2022-10-24   | 28       | 1         | 4        |
+-------------+--------------+----------+-----------+----------+
| 2022-10-25  | 2022-10-19   | 33       | 5         | 4        |
| 2022-10-25  | 2022-10-20   | 33       | 4         | 4        |
| 2022-10-25  | 2022-10-21   | 33       | 3         | 3        |   2022-10-24 计算
| 2022-10-25  | 2022-10-22   | 33       | 2         | 5        |
| 2022-10-25  | 2022-10-23   | 33       | 1         | 1        |
+-------------+--------------+----------+-----------+----------+


展示日期(first_login)
2022-10-19                        1   2   3   4   5   6   7
2022-10-20                        1   2   3   4   5   6
2022-10-21                        1   2   3   4   5

-- 留存分析报表

create  table  if not exists  doe34.ads_app_uac_retention_rept(
                                                                  compute_dt string ,
                                                                  first_login  string ,
                                                                  uac_cnt int  ,
                                                                  1_ret_day_cnt int ,
                                                                  2_ret_day_cnt int ,
                                                                  3_ret_day_cnt int ,
                                                                  4_ret_day_cnt int ,
                                                                  5_ret_day_cnt int ,
                                                                  6_ret_day_cnt int ,
                                                                  7_ret_day_cnt int
) stored as orc
    tblproperties ('orc.compress' = 'snappy') ;

insert  into  table  doe34.ads_app_uac_retention_rept
select
    '2022-10-26' as compute_dt ,
    t1.first_login ,
    t2.uac_cnt ,
    t1.`次日` ,
    t1.`第2日` ,
    t1.`第3日` ,
    t1.`第4日` ,
    t1.`第5日`,
    t1.`第6日`,
    t1.`第7日`
from
    (select
         first_login ,
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[0] , '-')[1],0)   as  `次日`   ,   --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[1] , '-')[1],0)  as  `第2日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[2] , '-')[1],0)  as  `第3日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[3] , '-')[1],0)  as  `第4日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[4] , '-')[1],0)  as  `第5日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[5] , '-')[1],0)  as  `第6日`   ,--  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[6] , '-')[1],0)  as  `第7日`    --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
     from
         doe34.test_dws_app_uac_retention where  comput_dt >= '2022-10-20'
     group  by  first_login) t1
        join
    -- 获取每个首登日当日注册的总人数
        (select
             register_date ,
             count(1)  as  uac_cnt
         from
             doe34.service_account_info
         where register_date   >=  '2022-10-19'
         group by register_date)t2
    on
            t1.first_login = t2.register_date
;

show  tables  from doe34 ;

desc formatted
    doe34.service_account_info ;

create table  tb_user_info(
                              guid  bigint ,
                              account string ,
                              register_date string
) ;

insert into  table tb_user_info  values
(10101 , 'zss' , '2022-10-21')  ,
(10102 , 'zss' , '2022-10-21')  ,
(10103 , 'zss' , '2022-10-22')  ,
(10104 , 'zss' , '2022-10-22')  ,
(10105 , 'zss' , '2022-10-23')  ,
(10105 , 'zss' , '2022-10-24')  ,
(10105 , 'zss' , '2022-10-24')  ,
(10105 , 'zss' , '2022-10-24')   ;



select
    '2022-10-26' as compute_dt ,
    t1.first_login ,
    t2.uac_cnt ,
    t1.`次日` ,
    t1.`第2日` ,
    t1.`第3日` ,
    t1.`第4日` ,
    t1.`第5日`,
    t1.`第6日`,
    t1.`第7日`
from
    (select
         first_login ,
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[0] , '-')[1],0)   as  `次日`   ,   --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[1] , '-')[1],0)  as  `第2日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[2] , '-')[1],0)  as  `第3日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[3] , '-')[1],0)  as  `第4日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[4] , '-')[1],0)  as  `第5日`  , --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[5] , '-')[1],0)  as  `第6日`   ,--  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
         nvl(split(collect_list(ret_days||'-'||ret_cnt)[6] , '-')[1],0)  as  `第7日`    --  [1-22  2-21 3-19 4-16 5-14 6-11 7-10]
     from
         doe34.test_dws_app_uac_retention where  comput_dt >= '2022-10-20'
     group  by  first_login) t1
        join
    -- 获取每个首登日当日注册的总人数
        (select
             register_date ,
             count(1)  as  uac_cnt
         from
             tb_user_info
         group by register_date)t2
    on
            t1.first_login = t2.register_date
;

