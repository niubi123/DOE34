package com.doitedu.test

/**
 * @Date: 22.11.10 
 * @Author: Hang.Nian.YY
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
object Test01 {
  def main(args: Array[String]): Unit = {
    val ls = List("e1" , "e2" , "e3" ,"e2" ,"e5" , "e3")

    val rng = 0 until ls.size
    val tuples = rng.zip(rng.reverse)
    val absValue = tuples.map(tp => {
      Math.abs(tp._1 - tp._2)
    })
    val fm = absValue.sum
    val res = absValue.map(v => {
      val res = v / fm.toDouble
      res
    })
  //  res.foreach(println)
    ls.zip(res).foreach(println)




  }

}
