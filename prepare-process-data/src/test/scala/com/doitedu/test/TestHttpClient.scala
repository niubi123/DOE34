package com.doitedu.test

import com.alibaba.fastjson.JSON
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.GetMethod

/**
 * @Date: 22.10.29 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
object TestHttpClient {
  def main(args: Array[String]): Unit = {

    val client = new HttpClient()
    val get = new GetMethod("https://restapi.amap.com/v3/geocode/regeo?output=json&location=118.07840793462009,32.78865847806049&key=220bd207a5b658b4ee04dceebe127738&radius=1000&extensions=base")
    val code = client.executeMethod(get)
    if (code == 200) {
      val res = get.getResponseBodyAsString
      val nObject = JSON.parseObject(res)
      val address = nObject.getJSONObject("regeocode")
                           .getJSONObject("addressComponent")
      val province = address.getString("province")
      var city = address.getString("city")
      if (city != null && city.startsWith("[")) {
        city = province
      }
      val district = address.getString("district")
      println(s"$province   $city   $district")

    }
  }

}
