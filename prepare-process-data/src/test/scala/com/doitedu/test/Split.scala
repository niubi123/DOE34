package com.doitedu.test

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import scala.collection.immutable

/**
 * @Date: 22.10.28 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
object Split {
  def main(args: Array[String]): Unit = {
    val session = SparkSession.builder()
      .enableHiveSupport()
      .master("local[*]")
      .appName("process-step01")
      .getOrCreate()

    val df = session.read.option("header",true).option("inferSchema" ,true).csv("data/split/")

    /**
     *
    root
     |-- id: integer (nullable = true)
     |-- event: string (nullable = true)
     |-- sessionid: string (nullable = true)
     |-- ts: integer (nullable = true)
     */
    val dataRDD  = df.rdd.map(row => {
      val id = row.getAs[Int]("id")
      val event = row.getAs[String]("event")
      val sessionId = row.getAs[String]("sessionid")
      val ts = row.getAs[Int]("ts")
     Bean(id, event, sessionId, ts ,"")
    })

    val grouped = dataRDD.groupBy(_.sessionId)
    grouped.flatMap(tp=>{
      val sessionId = tp._1
      val sorted = tp._2.toList.sortBy(_.ts)
      if(sorted !=null && sorted.size>0){
        var x = 0
        for (index <- 1 until sorted.size) {
          val preBean = sorted(index-1)
          preBean.new_session = sessionId+"-"+x
          val afterBean = sorted(index)
          val preTs = preBean.ts
          val afterTs = afterBean.ts
          if(afterTs - preTs > 30){  // session变化
            x+=1
            afterBean.new_session = sessionId+"-"+x
          }else{
            afterBean.new_session = sessionId+"-"+x
          }
        }
      }
      sorted
    }).foreach(println)




  }

}
