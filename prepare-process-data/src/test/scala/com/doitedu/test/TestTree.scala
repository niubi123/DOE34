package com.doitedu.test

import scala.collection.mutable.ListBuffer

/**
 * @Date: 22.11.4 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 *              根据页面的访问路径关系  构建树
 */
object TestTree {
  def main(args: Array[String]): Unit = {


    val O = TreeNode("/O" ,null)
    val F = TreeNode("/F" ,List[TreeNode](O))
    val E = TreeNode("/E" ,null)
    val B = TreeNode("/B" ,List[TreeNode](E,F))

    val P = TreeNode("/P" ,null)
    val C = TreeNode("/C" ,List[TreeNode](P))

    val L = TreeNode("/L" ,null)
    val U = TreeNode("/U" ,null)
    val T = TreeNode("/T" ,null)
    val M = TreeNode("/M" ,List[TreeNode](L,U,T))
    val D = TreeNode("/D" ,List[TreeNode](M))

    val A = TreeNode("/A" ,List[TreeNode](B,C,D))
    val buffer = ListBuffer[(String, Int)]()
    getFollowPv2(A , buffer)

    for (elem <- buffer) {
      println(elem)
    }


  }

  /**
   * 返回每个节点下的子节点个数
   * List[(String,Int)]
   */
  def getFollowPv2(treeNode: TreeNode , listBuffer:ListBuffer[(String,Int)]): Int ={
    var pv = 0

    val children = treeNode.children
    if(children!=null && children.size >0){
      pv +=children.size
      for (elem <- children) {
        pv+= getFollowPv2(elem,listBuffer)
      }
    }
    // 当前节点 添加到缓存中
    listBuffer.append((treeNode.url ,pv))
    pv
  }



  /**
   * 指定节点的所有的子...子节点个数
   * @param treeNode
   * @return
   */
  def getFollowPv(treeNode: TreeNode): Int ={
    var pv = 0
    val list = treeNode.children
    if(list!=null && list.size>0){  // 说明有子节点
      pv+=list.size  // 3
      for (elem <- list) {
       pv+= getFollowPv(elem)
      }
    }
    pv
  }


}