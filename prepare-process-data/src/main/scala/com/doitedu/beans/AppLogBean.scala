package com.doitedu.beans

/**
 * @Date: 22.10.28 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
case class AppLogBean(
 account:String ,
 appId:String ,
 appVersion :String ,
 carrier:String ,
 deviceId:String ,
 deviceType :String ,
 eventId :String ,
 ip:String ,
 latitude :Double ,
 longitude :Double ,
 netType:String ,
 osName :String ,
 osVersion:String ,
 properties :Map[String,String] ,
 releaseChannel:String ,
 resolution :String ,
 var sessionId :String ,  // dedtrtr3424
 var new_session:String ,
 ts:Long ,
 dt:String )
