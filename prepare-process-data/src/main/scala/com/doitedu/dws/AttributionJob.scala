package com.doitedu.dws

import java.lang.StringBuffer

import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

import scala.collection.{immutable, mutable}
import scala.collection.mutable.ListBuffer

/**
 * @Date: 22.11.10 
 * @Author: Hang.Nian.YY
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
object AttributionJob {
  Logger.getLogger("org").setLevel(Level.ERROR)
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME", "root")

    val session = SparkSession.builder()
      .master("local[*]")
      .appName("acc_page_wide")
      .enableHiveSupport()
      .getOrCreate()
    import session.implicits._
    import org.apache.spark.sql.functions._

    /**
     * (用户行为数据)
     * 加载 事件数据
     * 过滤
     */
    //val  df =  session.read.table("doe34.test_event_funnel")/*.where(dt='2022-10-26')*/

    val df = session.sql(
      """
        |select
        |t1.guid ,
        |t2.event_id ,
        |t2.ts
        |from
        |(
        |select
        |guid
        |from
        |doe34.test_event_funnel
        |where
        |event_id = 'e4' and properties['p2'] = 'v2'
        |group by guid
        |) t1
        |join
        |(
        |select
        |guid ,
        |event_id ,
        |properties ,
        |ts
        |from
        |doe34.test_event_funnel
        |where event_id in ('e1' , 'e2' , 'e3' , 'e5')
        |or  (event_id = 'e4' and properties['p2'] = 'v2' )
        |)t2
        |on  t1.guid = t2.guid
        |""".stripMargin)

    //  将数据按照用户分组   按照时间排序
    val grouped = df.rdd.map(row => {
      val guid = row.getAs[Long]("guid")
      val event_id = row.getAs[String]("event_id")
      val ts = row.getAs[Long]("ts")
      (guid, event_id, ts)
    }).groupBy(_._1)

    /**
     * guid1001  ,  e1  e2  e1  e2 e3  -->   e4
     * guid1001  ,  e1  e2  e1         -->   e4
     * guid1001  ,  e1  e2 e3
     */
    val eventsSequence: RDD[ListBuffer[ListBuffer[String]]] = grouped.map(tp => {
      val guid = tp._1
      // 时间排序
      val sorted = tp._2.toList.sortBy(_._3)
      // 处理事件
      val lss = ListBuffer[ListBuffer[String]]()
      var ls = ListBuffer[String]()
      for (event <- sorted) {
        if (!"e4".equals(event._2)) {
          ls.append(event._2)
        } else {
          lss.append(ls)
          ls = ListBuffer[String]()
        }
      }
      lss
    })


    /**
     *  归因分析  首次触点归因
      */

    // guid01    ls1 e4    ls2  e4
    val res = eventsSequence.flatMap(lss => {
      //lss:  guid01 --> [e3  e1  e2  e3]   [e2  e1  e3]
      for (ls <- lss) yield {
        //[e3 100%  e1 0%  e2 0%  e3 %0]
        val sb = new StringBuffer()
        for (i<- 0 until  ls.size) {
          if(i == 0){
            sb.append(s"${ls(i)}-100% ")
          }else{
            sb.append(s"${ls(i)}-0% ")
          }
        }
        sb.toString.trim
      }
    })

    /**
     *  归因分析  末次触点归因
     */
    val res2 = eventsSequence.flatMap(lss => {
      //lss:  guid01 --> [e3  e1  e2  e3]   [e2  e1  e3]
      for (ls <- lss) yield {
        //[e3 100%  e1 0%  e2 0%  e3 %0]
        val sb = new StringBuffer()
        for (i<- 0 until  ls.size) {
          if(i == ls.size-1){
            sb.append(s"${ls(i)}-100% ")
          }else{
            sb.append(s"${ls(i)}-0% ")
          }
        }
        sb.toString.trim
      }
    })


    /**
     * 线性归因
     */

    val res3 = eventsSequence.flatMap(lss => {
      //lss:  guid01 --> [e3  e1  e2  e3]   [e2  e1  e3]
      for (ls <- lss) yield {
        //[e3 100%  e1 0%  e2 0%  e3 %0]
        val sb = new StringBuffer()
        val fm = ls.size
        for (i<- 0 until  ls.size) {
            sb.append(s"${ls(i)}:"+(1*100/fm)+"% ")
        }
        sb.toString.trim
      }
    })

    /**
     * 位置归因
     * 首次占比  事件            事件   末次占比
     * [e1  e2  e3  e2  e1  e5]
     * 0   1   2   3   4   5   6
     * 6   5   4   3   2   1   0
     * 6   4   2   0   2   4   6
     *
     */

    val res4 = eventsSequence.flatMap(lss => {
      //lss:  guid01 --> [e3  e1  e2  e3]   [e2  e1  e3]
      for (ls <- lss) yield {
        val range = 0 until ls.size
        val ints = range.zip(range.reverse).map(tp => {
          Math.abs(tp._1 - tp._2)
        })
        val fm = ints.sum
        val tuples: immutable.Seq[(String, String)] = ints.map(int => {
          ((int.toDouble / fm.toDouble) * 100).toInt + "%"
        }).zip(ls)
        tuples
      }
    })

    /**
     * 时间衰减
     */

    val res5 = eventsSequence.flatMap(lss => {
      //lss:  guid01 --> [e3  e1  e2  e3]   [e2  e1  e3]
      for (ls <- lss) yield {
        val range = 1 to ls.size
        val fm = range.sum
        val tuples = range.map(e => {
          ((e.toDouble / fm.toDouble) * 100).toInt + "%"
        }).zip(ls)
        tuples
      }
    })


    /**
     * 自定义归因
     *    1) 一个目标事件中 代归因事件不能重复  去重
     *    2)  e2事件  50%    省下的事件线性分配权重
     *
     *    [e1  e2  e3  e4]
     */
  val  res6 =  eventsSequence.flatMap(lss => {
      for (ls <- lss) yield {
        // 去重待归因事件中的重复事件
        val distincted = ls.distinct
        val  data: mutable.Seq[(String, String)] = for (elem <- distincted) yield {
          val maybeString = distincted.find(_.equals("e2"))
          if(maybeString.isDefined){
            if ("e2".equals(elem)) {
              ("50%", elem)
            } else {
              (50 / (distincted.size - 1) + "%", elem)
            }
          }else{
            ((100/distincted.size) +"%" ,elem)
          }

        }
        data
      }
    })




    res6.foreach(println)
    session.close()


  }

}
