package com.doitedu.dws

/**
 * @Date: 22.11.4 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
case class PageBean(page_url: String, guid: Long, session_id: String, ref_url: String, ref_type: String, var follow_pv:Int , acc_long: Long, ts: Long) {

}
