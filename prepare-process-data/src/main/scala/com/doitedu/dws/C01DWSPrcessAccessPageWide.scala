package com.doitedu.dws

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SaveMode, SparkSession}

import scala.collection.immutable
import scala.collection.mutable.ListBuffer

/**
 * @Date: 22.11.4 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 */
object C01DWSPrcessAccessPageWide {

  def  getFollowPv(root:TreeNode , listBuffer: ListBuffer[(PageBean , Int)]): Int ={
    var pv = 0
    val children = root.children
    if(children!=null  && children.size>0){
      pv += children.size
      for (child <- children) {
        pv+=getFollowPv(child ,listBuffer)
      }
    }
    listBuffer.append((root.pageBean ,pv))
    pv
  }




  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME" , "root")

    val session = SparkSession.builder()
      .master("local[*]")
      .appName("acc_page_wide")
      .enableHiveSupport()
      .getOrCreate()
    import session.implicits._
    import org.apache.spark.sql.functions._

    /**
     * -------------------------------------------------------------------------------------------------------------
     * 第一步   加载页面明细数据     为页面明细计算下游贡献量                                                                                    ||
     * -------------------------------------------------------------------------------------------------------------
     */
    val pageDtl = session.read.table("doe34.dws_flow_acc_page_dtl").where("dt='2022-10-26'")
    val pageBeans: RDD[PageBean] = pageDtl.rdd.map(row => {
      val page_url = row.getAs[String]("page_url")
      val guid = row.getAs[Long]("guid")
      val session_id = row.getAs[String]("session_id")
      val ref_url = row.getAs[String]("ref_url")
      val ref_type = row.getAs[String]("ref_type")
      val acc_long = row.getAs[Long]("acc_long")
      val ts = row.getAs[Long]("ts")
      PageBean(page_url, guid, session_id, ref_url, ref_type, 0 , acc_long, ts)
    })
    // 按照session分组
    val grouped: RDD[(String, Iterable[PageBean])] = pageBeans.groupBy(_.session_id)

    val roots: RDD[TreeNode] = grouped.flatMap(tp => {
      val sessionId = tp._1
      val sorted: immutable.Seq[PageBean] = tp._2.toList.sortBy(_.ts)
      var trees: ListBuffer[ListBuffer[TreeNode]] = ListBuffer[ListBuffer[TreeNode]]()
      var tree: ListBuffer[TreeNode] = null
      if (sorted != null && sorted.size > 0) {
        for (pageBean <- sorted) {  // 遍历会话内部每个访问的页面
          // 将当前一条的页面信息数据  构建一个节点
          val currentNode = TreeNode(pageBean, ListBuffer[TreeNode]())
          // 是否构建新的树
          if (tree == null || pageBean.ref_url == null || pageBean.ref_url.trim == "" || pageBean.ref_url.startsWith("http")) {
            // 创建树
            tree = ListBuffer[TreeNode]()
            // 将当前树添加到本次会话的多个树中   因为一次会话可能出现多个树的情况
            trees += tree
            // 将当前的节点   添加到树节点列表中
            tree += currentNode
          } else {
            // 将当前的节点   添加到树节点列表中
            tree += currentNode
            //  为每个页面  构建成节点  将节点添加到指定的树中 此时 没有节点和节点之间的关系
            //------------------------------------------------------------

            // 从最新的页面开始匹配  当前页面的来源页面    ref_url  = page_url
            val maybeNode = tree.reverse.find(node => node.pageBean.page_url.equals(currentNode.pageBean.ref_url))
            // 匹配上
            if (maybeNode.isDefined) {
              val node: TreeNode = maybeNode.get
              // 将当前节点挂载到  匹配页面的子节点上
              node.children += currentNode
            } else {  //  当前页面 在当前的树的所有节点上没有匹配到父节点  (数据问题)
              if (tree.size > 1) { // 只有两个节点以后 才会找父节点
                // 将随后一个节点 ,也就是当前节点 , 挂载到倒数第二个节点
                tree(tree.size - 2).children += currentNode
              }
            }
          }
        }
      }
      val nodes = trees.map(tree => tree.head)
      nodes
    })

    val pageBean = roots.flatMap(root => { // TreeNode
      val buffer = ListBuffer[(PageBean, Int)]()
      getFollowPv(root, buffer)
      buffer.map(tp => {
        val bean = tp._1
       PageBean(bean.page_url, bean.guid, bean.session_id, bean.ref_url, bean.ref_type,tp._2 , bean.acc_long, bean.ts)
      })
    })

    val pageDtlDF = pageBean.toDF()
   //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    pageDtlDF.createOrReplaceTempView("page_dtl")

    /**
     * root
     * |-- page_url: string (nullable = true)
     * |-- guid: long (nullable = false)
     * |-- session_id: string (nullable = true)
     * |-- ref_url: string (nullable = true)
     * |-- ref_type: string (nullable = true)
     * |-- follow_pv: integer (nullable = false)
     * |-- acc_long: long (nullable = false)
     * |-- ts: long (nullable = false)
     */
    /**
     * select
     * if(split(max(ts||'-'||page_url) over(partition by session_id) , '-')[1] == page_url  ,  1,0) as is_exit ,
     * if(split(min(ts||'-'||page_url) over(partition by session_id) , '-')[1] == page_url  ,  1,0) as is_enter,
     * from
     * page_dtl
     */


 /*  a,session01,101
    b,session01,201
    f,session01,301
    j,session01,401
    k,session01,602

    create table tb_test_exit(
      page_url  string,
      session_id  string,
      ts bigint
    )
    row format delimited  fields terminated by  ',' ;

    load data  local  inpath  '/data/a.exit' into   table tb_test_exit ;
    select * from tb_test_exit ;


    select
    *  ,
    if ( row_number() over (partition by session_id order by ts)  == 1 , 1, 0) as  is_enter ,
    if ( row_number() over (partition by session_id order by ts desc )  == 1 , 1, 0) as  is_exit
      from tb_test_exit;


    --

    select
    * ,
    if(page_url == split(max(ts||'-'||page_url)  over (partition by session_id) ,'-')[1] , 1, 0)  as  is_exit ,
    if(page_url == split(min(ts||'-'||page_url)  over (partition by session_id) ,'-')[1] , 1, 0)  as  is_enter
      from tb_test_exit;*/




    /**
     * -------------------------------------------------------------------------------------------------------------
     * 第二步   加载用户轻度聚合表                                                                                 ||
     * -------------------------------------------------------------------------------------------------------------
     */
    session.sql(
      """
        |SELECT
        |    t1.page_url,
        |    t1.guid,
        |    t1.session_id,
        |    t1.ref_url,
        |    t1.ref_type,
        |    t2.is_new,
        |    t1.follow_pv,
        |    t1.is_exit,
        |    t1.is_enter,
        |    t1.acc_long,
        |    t2.province,
        |    t2.city,
        |    t2.area,
        |    t1.ts ,
        |    '2022-10-26' dt
        |FROM
        |    (
        |        SELECT
        |            page_url,
        |            guid,
        |            session_id,
        |            ref_url,
        |            ref_type,
        |            follow_pv,
        |            acc_long,
        |            if (
        |                ROW_NUMBER() OVER (
        |                    PARTITION BY session_id
        |                    ORDER BY
        |                        ts desc
        |                ) == 1,
        |                1,
        |                0
        |            ) AS is_exit,
        |            if (
        |                ROW_NUMBER() OVER (
        |                    PARTITION BY session_id
        |                    ORDER BY
        |                        ts
        |                ) == 1,
        |                1,
        |                0
        |            ) AS is_enter,
        |            ts
        |        FROM
        |            page_dtl
        |    ) t1
        |    JOIN (
        |        SELECT
        |            guid,
        |            is_new,
        |            province,
        |            city,
        |            area
        |        FROM
        |            doe34.dws_flow_acc_page_user_agg
        |        WHERE
        |            dt = '2022-10-26'
        |    ) t2 ON t1.guid = t2.guid
        |""".stripMargin).write.format("orc").mode(SaveMode.Overwrite).partitionBy("dt").saveAsTable("doe34.dws_flow_acc_page_wide")



    /**
     * root
     * |-- page_url: string (nullable = true)
     * |-- guid: long (nullable = true)
     * |-- session_id: string (nullable = true)
     * |-- ref_url: string (nullable = true)
     * |-- ref_type: string (nullable = true)
     * |-- acc_long: long (nullable = true)
     * |-- ts: long (nullable = true)
     * |-- dt: string (nullable = true)
     */

    /**
     * ----------------------------------------------------------------
     * 第一步   加载页面明细数据
     * page_url  string ,  √
     * guid bigint , √
     * session_id  string , √
     * ref_url string , √
     * ref_type  string , √
     * is_new int  , --  X*
     * follow_pv  int ,  √
     * is_exit_page int ,  X
     * is_enter_page int   X
     * acc_long  bigint  √
     * province  string --  X*
     * city  string  --     X*
     * area  string   --  X*
     * ts  bigint      √
     */


    /**
     * 关联用户轻度聚合表 is_new
     */

    session.close()

  }

}

