package com.doitedu.dws

import scala.collection.mutable.ListBuffer

/**
 * @Date: 22.11.4 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:页面信息构建节点
 */
case  class  TreeNode(pageBean:PageBean , children: ListBuffer[TreeNode])