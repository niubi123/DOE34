package com.doitedu.process

import java.sql.DriverManager
import java.util
import java.util.Properties

import ch.hsr.geohash.GeoHash
import com.alibaba.fastjson.JSON
import com.doitedu.beans.AppLogBean2
import org.apache.commons.httpclient.HttpClient
import org.apache.commons.httpclient.methods.GetMethod
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{Row, SaveMode, SparkSession}

/**
 * @Date: 22.10.29 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 *              加载session分割后的数据 根据经纬度 集成地理位置信息
 *              存储在表 tmp_app_log_split_areas
 *
 */
object C01_Process_Step02 {
  def main(args: Array[String]): Unit = {
    System.setProperty("HADOOP_USER_NAME" , "root")
    // 1 获取session对象
    val session = SparkSession.builder()
      .enableHiveSupport()
     // .master("local[*]")
     // .appName("process-step02")
      .getOrCreate()
    import session.implicits._
    val df = session.read.table("doe34.tmp_app_log_split").where(s"dt='${args(0)}'")
    // 直接加载hive中的数据
    /*df.printSchema()
    df.show()*/
    /**
     * root
     * |-- account: string (nullable = true)
     * |-- app_id: string (nullable = true)
     * |-- app_version: string (nullable = true)
     * |-- carrier: string (nullable = true)
     * |-- device_id: string (nullable = true)
     * |-- device_type: string (nullable = true)
     * |-- event_id: string (nullable = true)
     * |-- ip: string (nullable = true)
     * |-- latitude: double (nullable = true)
     * |-- longitude: double (nullable = true)
     * |-- net_type: string (nullable = true)
     * |-- os_name: string (nullable = true)
     * |-- os_version: string (nullable = true)
     * |-- properties: map (nullable = true)
     * |    |-- key: string
     * |    |-- value: string (valueContainsNull = true)
     * |-- release_channel: string (nullable = true)
     * |-- resolution: string (nullable = true)
     * |-- session_id: string (nullable = true)
     * |-- new_session: string (nullable = true)
     * |-- ts: long (nullable = true)
     * |-- dt: string (nullable = true)
     */

     // 2  加载地理位置字典表  广播
     val props = new Properties()
    props.setProperty("user" , "root")
    props.setProperty("password" , "root")
    val dict = session.read.jdbc("jdbc:mysql://windows:3306/doit34?useSSL=false",  "tb_areas_dict", props)
   val  dictMap: Map[String, (String, String, String)] = dict.rdd.map(row=>{
     (row.getAs[String]("geo_key") ,
       (row.getAs[String]("province"),row.getAs[String]("city"),row.getAs[String]("area")))
   }).collect().toMap
    /**
     * 将数据广播 , RDD   DataFrame是不能广播
     * 收集数据  List结构  Map结构
     */
    val sc = session.sparkContext
    val bc = sc.broadcast(dictMap)


    //1 解析数据  获取经纬度

    val res: RDD[AppLogBean2] = df.rdd.mapPartitions(iters => {
      val conn = DriverManager.getConnection("jdbc:mysql://windows:3306/doit34?useSSL=false&characterEncoding=utf8", "root", "root")
      val ps = conn.prepareStatement("insert into  tb_areas_dict values(?,?,?,?,?,?)")
      iters.map(row => {
        val account = row.getAs[String]("account")
        val app_id = row.getAs[String]("app_id")
        val app_version = row.getAs[String]("app_version")
        val carrier = row.getAs[String]("carrier")
        val device_id = row.getAs[String]("device_id")
        val device_type = row.getAs[String]("device_type")
        val ip = row.getAs[String]("ip")
        val net_type = row.getAs[String]("net_type")
        val os_name = row.getAs[String]("os_name")
        val os_version = row.getAs[String]("os_version")
        val properties = row.getAs[Map[String, String]]("properties")
        val release_channel = row.getAs[String]("release_channel")
        val event_id = row.getAs[String]("event_id")
        val resolution = row.getAs[String]("resolution")
        val session_id = row.getAs[String]("session_id")
        val new_session = row.getAs[String]("new_session")
        val latitude = row.getAs[Double]("latitude")
        val longitude = row.getAs[Double]("longitude")
        val ts = row.getAs[Long]("ts")
        val dt = row.getAs[String]("dt")

        // 根据经纬度获取地理位置信息  字典表  mysql  获取广播数据
        val dictMap = bc.value
        val key = GeoHash.geoHashStringWithCharacterPrecision(latitude, longitude, 6)

        val maybeTuple = dictMap.get(key)
        //var tp = ("", "" ,"")
        var bean: AppLogBean2 = null
        if (maybeTuple.isDefined) { // 本地字典库中有地理位置  集成
          val tp = maybeTuple.get
          bean = AppLogBean2(0L, account, app_id, app_version, carrier, device_id, device_type, event_id, ip, latitude, longitude, net_type, os_name, os_version, properties, release_channel, resolution, session_id, new_session, tp._1, tp._2, tp._3, ts, dt)
        } else { // 本地字典库中没有地理位置   请求网络  集成
          // 通过网络请求  第三方 接口 (API)
          // https://restapi.amap.com/v3/geocode/regeo?output=json&location=116.310003,39.991957&key=220bd207a5b658b4ee04dceebe127738&radius=1000&extensions=base
          val client = new HttpClient()
          val get = new GetMethod(s"https://restapi.amap.com/v3/geocode/regeo?output=json&location=$longitude,$latitude&key=220bd207a5b658b4ee04dceebe127738&radius=1000&extensions=base")
          val code = client.executeMethod(get)
          if (code == 200) {
            try {
              val res = get.getResponseBodyAsString
              val nObject = JSON.parseObject(res)
              val address = nObject.getJSONObject("regeocode")
                .getJSONObject("addressComponent")
              val province = address.getString("province")
              var city = address.getString("city")
              if (city != null && city.startsWith("[")) {
                city = province
              }
              val district = address.getString("district")
              bean = AppLogBean2(0L, account, app_id, app_version, carrier, device_id, device_type, event_id, ip, latitude, longitude, net_type, os_name, os_version, properties, release_channel, resolution, session_id, new_session, province, city, district, ts, dt)
              //  丰富本地的地理位置字典库

              ps.setString(1, key)
              ps.setString(2, province)
              ps.setString(3, city)
              ps.setString(4, district)
              ps.setDouble(5, latitude)
              ps.setDouble(6, longitude)
              ps.execute()
            } catch {
              case e:Exception => bean = null
            }
          }
        }
        bean
      })
    })

    res.toDF(
     "guid" ,
      "account" ,
      "app_id" ,
      "app_version" ,
      "carrier" ,
      "device_id" ,
      "device_type" ,
      "event_id" ,
      "ip" ,
      "latitude" ,
      "longitude" ,
      "net_type" ,
      "os_name" ,
      "os_version" ,
      "properties" ,
      "release_channel" ,
      "resolution" ,
      "session_id" ,
      "new_session" ,
      "province" ,
      "city" ,
      "area" ,
      "ts" ,
      "dt"
    ).write
      .format("orc")
      .partitionBy("dt")
      .mode(SaveMode.Overwrite)
      .saveAsTable("doe34.tmp_app_log_split_areas")

}

}
