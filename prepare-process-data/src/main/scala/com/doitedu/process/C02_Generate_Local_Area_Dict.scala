package com.doitedu.process

import java.util.Properties

import ch.hsr.geohash.GeoHash
import org.apache.spark.sql.SparkSession

/**
 * @Date: 22.10.29 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 *              字典
 */
object C02_Generate_Local_Area_Dict {
  def main(args: Array[String]): Unit = {
    /**
     * 有些地理位置数据存储在mysql中  ,使用这个物料数据 构建地理位置字典表
     * 将经纬度  构架一个 GEOHASH 字符串 作为查询依据
     *     JDBC:
     * 加载MySQL
     * 构建视图
     * 自定义函数    GEOHASH_STR
     * 输出结果
     */

    //1 加载MySQL
    val session = SparkSession.builder()
      .master("local[*]")
      .appName("process-step02")
      .getOrCreate()
    val props = new Properties()
    props.setProperty("user" , "root")
    props.setProperty("password" , "root")

    val df = session.read.jdbc("jdbc:mysql://localhost:3306/doit34?useSSL=false", "t_md_areas", props)
    // 定义函数  将经纬度转换成一个字符串
    val f = (lat:Double , lng:Double)=>{
      val str = GeoHash.geoHashStringWithCharacterPrecision(lat, lng, 6)
      str
    }
   // 注册
    session.udf.register("geo_str" , f)
    df.createOrReplaceTempView("t_md_areas")
    session.sql(
      """
        |SELECT
        |geo_str(t3.BD09_LAT, t3.BD09_LNG)  as geo_key ,
        |t1.AREANAME as province ,
        |t2.AREANAME as city ,
        |t3.AREANAME as area  ,
        |t3.BD09_LAT as  lat  ,
        |t3.BD09_LNG as lng
        |FROM
        |t_md_areas  AS  t3
        |join
        |t_md_areas  AS t2
        |on t3.PARENTID = t2.ID AND t3.`LEVEL` = '3'
        |join t_md_areas  AS t1
        |on  t2.PARENTID = t1.ID
        |union
        |-- 四级行政单位
        |SELECT
        |geo_str(t4.BD09_LAT ,t4.BD09_LNG ) as geo_key,
        |t1.AREANAME as province,
        |t2.AREANAME city,
        |t3.AREANAME area,
        |t4.BD09_LAT as lat  ,
        |t4.BD09_LNG as lng
        |FROM
        |t_md_areas t4
        |join
        |t_md_areas t3
        |on t4.PARENTID = t3.ID AND  t4.`LEVEL` = '4'
        |join
        |t_md_areas t2
        |on t3.PARENTID = t2.ID
        |join
        |t_md_areas t1
        |ON t2.PARENTID = t1.ID
        |""".stripMargin)
       // 将字典数据存储在mysql中
      .write.jdbc("jdbc:mysql://localhost:3306/doit34?useSSL=false&characterEncoding=utf8", "tb_areas_dict", props)



  }

}
