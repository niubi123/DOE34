package com.doitedu.process

import com.doitedu.beans.AppLogBean
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{SaveMode, SparkSession}

/**
 * @Date: 22.10.28 
 * @Author: HANGGE
 * @qq: 598196583
 * @Tips: 学大数据 ,到多易教育
 * @Description:
 * 数据预处理
 * 第一步 : session分割后的数据  存储在表 tmp_app_log_split
 * 1) 加载hive ods层中的数据
 * 2) 核心字段缺失 , 统一数据格式
 * 3) 统一命名规范 输出时
 */
object C01_Process_Step01 {

  def main(args: Array[String]): Unit = {


    System.setProperty("HADOOP_USER_NAME" , "root")
    // 1 获取session对象
    val session = SparkSession.builder()
      .enableHiveSupport()
      //.master("local[*]")
      // .appName("process-step01")
      .getOrCreate()
    import session.implicits._
    // 直接加载hive中的数据
    val frame = session.read.table("doe34.ods_app_event_log").where(s"dt='${args(0)}'")
    /*    session.sql(
          """
            |直接操作hive表
            |where dt = '2022-10-26'
            |and  (sessionid is not null and trim(sessionid) != '')
            |or  (ip is not null and trim(ip) != '')
            |or  timestamp != 0
            |""".stripMargin)*/
    /**
     * root
     * |-- account: string (nullable = true)
     * |-- appid: string (nullable = true)
     * |-- appversion: string (nullable = true)
     * |-- carrier: string (nullable = true)
     * |-- deviceid: string (nullable = true)
     * |-- devicetype: string (nullable = true)
     * |-- eventid: string (nullable = true)
     * |-- ip: string (nullable = true)
     * |-- latitude: double (nullable = true)
     * |-- longitude: double (nullable = true)
     * |-- nettype: string (nullable = true)
     * |-- osname: string (nullable = true)
     * |-- osversion: string (nullable = true)
     * |-- properties: map (nullable = true)
     * |    |-- key: string
     * |    |-- value: string (valueContainsNull = true)
     * |-- releasechannel: string (nullable = true)
     * |-- resolution: string (nullable = true)
     * |-- sessionid: string (nullable = true)
     * |-- timestamp: long (nullable = true)
     * |-- dt: string (nullable = true)
     */

    // 过滤核心字段缺失的数据    使用spark-core编程  将DF 转成 RDD
    val logbeans  = frame.rdd.map(row => {
      val account = row.getAs[String]("account")
      val appid = row.getAs[String]("appid")
      val appversion = row.getAs[String]("appversion")
      val carrier = row.getAs[String]("carrier")
      val deviceid = row.getAs[String]("deviceid")
      val devicetype = row.getAs[String]("devicetype")
      val eventid = row.getAs[String]("eventid")
      val ip = row.getAs[String]("ip")
      val latitude = row.getAs[Double]("latitude")
      val longitude = row.getAs[Double]("longitude")
      val nettype = row.getAs[String]("nettype")
      val osname = row.getAs[String]("osname")
      val osversion = row.getAs[String]("osversion")
      val properties = row.getAs[Map[String, String]]("properties")
      val releasechannel = row.getAs[String]("releasechannel")
      val resolution = row.getAs[String]("resolution")
      val sessionid = row.getAs[String]("sessionid")
      val timestamp = row.getAs[Long]("timestamp")
      val dt = row.getAs[String]("dt")
      // 将解析的属性 封装成Bean
    val bean =   AppLogBean(account
        , appid
        , appversion
        , carrier
        , deviceid
        , devicetype
        , eventid
        , ip
        , latitude
        , longitude
        , nettype
        , osname
        , osversion
        , properties
        , releasechannel
        , resolution
        , sessionid
        , ""
        , timestamp
        , dt
      )
      bean
    })


    // 去重脏数据
  val filtered = logbeans.filter(_!=null)
    .filter(bean=>bean.sessionId != null &&  bean.sessionId.trim !="")
    .filter(bean=>bean.ip != null &&  bean.ip.trim !="")
    .filter(bean=> (bean.ts+"").length == 13)
    // 统一数据格式  . 将sessionid 大写
    /**
     * 第一件事:
     * 加载hive ods层中的数据
     * 核心字段缺失 , 统一数据格式
     * 统一命名规范 输出时
     *    此时数据可以输出  生成一个临时表  doe34.app_event_log_step01
     */

    val resRDD = filtered.map(bean => {
      bean.sessionId = bean.sessionId.toUpperCase()
      bean
    })

    /**
     * 第二件事 : session分割
     * 处理的数据是app域数据 , 这种日志数据没有对session进行分割
     *      统计用户的访问次数 ,访问时长 , 不准确  :   挂起的程序
     * 在一个session中 如果一个事件 经过30分钟没有后续的操作  , 将session断开
     * uid001,zss,A,1,sessionid001,sessopmid001-01
     * uid001,zss,B,2,sessionid001,sessopmid001-01
     * uid001,zss,C,3,sessionid001,sessopmid001-01   -- 两个事件的差值大于阈值 断开
     * uid001,zss,D,60,sessionid001,sessopmid001-02
     *
     * uid002,lss,A,1,sessionid001,sessopmid002
     * uid002,lss,B,2,sessionid001,sessopmid002
     * uid002,lss,C,3,sessionid001,sessopmid002
     * uid002,lss,D,40,sessionid001,sessopmid003
     *      select
     *      uid ,
     *      session_id ,
     *      ts ,
     *      concat(session_id , '-' ,  sum(flag) over(partition by  uid , session_id  order by ts)) as new_session
     *      from
     *      (
     *      select
     *      uid ,
     *      session_id ,
     *      ts,
     *      if((ts - lag(ts ,1 , ts)  over(partition by uid , session_id order by ts))>30 , 1, 0 ) as flag
     *      from
     *      doe34.test_split_session
     *      ) t ;
     *
     *  使用spark代码编写
     *  按照原来的sessionid分组  ,按照时间排序
     *
     *  DGSJDGFFS: [DGSJDGFFS_1 , DGSJDGFFS_2 , DGSJDGFFS_3 , DGSJDGFFS_40 ,DGSJDGFFS_50]
     *              DGSJDGFFS-1   DGSJDGFFS-1   DGSJDGFFS-1    DGSJDGFFS-2   DGSJDGFFS-2
     *
     */
      // 按照sessionid分组
    val grouped = resRDD.groupBy(_.sessionId)


    val splitedRes: RDD[AppLogBean] = grouped.flatMap(tp=>{
      val sessionid = tp._1
      val sorted = tp._2.toList.sortBy(_.ts)
      if(sorted!=null && sorted.size>0){
        var x = 0
        for (index <- 1 until sorted.size) {
          val preBean = sorted(index-1)
          preBean.new_session = sessionid+"-"+x
          val afterBean = sorted(index)
          val preTs = preBean.ts
          val afterTs = afterBean.ts
          if(afterTs - preTs > 1000*18){  // session变化
         // if(afterTs - preTs > 1000*60*30){  // session变化
             x+=1
            afterBean.new_session = sessionid+"-"+x
          }else{
            afterBean.new_session = sessionid+"-"+x
          }
        }
      }
      sorted
    })

   // 将切割好的数据 输出中间结果   HIVE
    // 将数据保存到hive中  数据集成  由于这张是一个临时中间结果表 , 字段不规范
   val resDF = splitedRes.toDF(
      "account" ,
      "app_id" ,
      "app_version" ,
      "carrier" ,
      "device_id" ,
      "device_type" ,
      "event_id" ,
      "ip" ,
      "latitude" ,
      "longitude" ,
      "net_type" ,
      "os_name" ,
      "os_version" ,
      "properties" ,
      "release_channel" ,
      "resolution" ,
     "session_id" ,
     "new_session" ,
     "ts" ,
      "dt"
    )
    resDF
      .write
      .format("orc")
      .partitionBy("dt")
      .mode(SaveMode.Overwrite)
      .saveAsTable("doe34.tmp_app_log_split")
    session.close()


  }
}
